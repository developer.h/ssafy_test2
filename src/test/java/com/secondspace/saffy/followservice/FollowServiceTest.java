package com.secondspace.saffy.followservice;

import com.secondspace.saffy.dao.FollowDao;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.dao.UserPrivacyDao;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.vo.model.Follow;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FollowServiceTest {
    @InjectMocks
    FollowService followService;

    @Mock
    FollowDao followDao;
    @Mock
    UserDao userDao;
    @Mock
    UserPrivacyDao userPrivacyDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void isFollowingTest() {
        Follow follow = new Follow();
        follow.setUid("test1");
        follow.setTargetUid("test2");
        follow.setStatus(Follow.Status.APPROVED);
        follow.setId(1);

        when(followDao.getByUidAndTargetUidAndStatus("test1", "test2", Follow.Status.APPROVED)).thenReturn(follow);
        boolean result = followService.isFollowing("test1", "test2");

        Follow follow1 = followDao.getByUidAndTargetUidAndStatus("test1", "test2", Follow.Status.APPROVED);

        assertTrue(follow1 != null && result);
    }

}
