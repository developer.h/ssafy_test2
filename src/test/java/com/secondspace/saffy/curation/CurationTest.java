//package com.secondspace.saffy.curation;
//
//import com.secondspace.saffy.service.CurationCrawler;
//import com.secondspace.saffy.vo.model.Article;
//import com.secondspace.saffy.vo.model.SearchSource;
//import lombok.extern.slf4j.Slf4j;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.Arrays;
//import java.util.Collection;
//
//@Slf4j
//@SpringBootTest
//public class CurationTest {
//    @Autowired
//    private CurationCrawler curationCrawler;
//
//    @Test
//    public void rssCurationTest() {
//        Collection<Article> result = curationCrawler.startCurationCrawling(Arrays.asList("모니터링", "기술")
//                , Arrays.asList(SearchSource.builder().rssUrl("https://kingbbode.tistory.com/rss").type(SearchSource.Type.RSS).build()));
//
//        log.info("rssCurationTest : {}"+ result);
//    }
//    @Test
//    public void gceCurationTest() {
//        Collection<Article> result = curationCrawler.startCurationCrawling(Arrays.asList("모니터링", "기술")
//                , Arrays.asList(SearchSource.builder().gceApiKey("AIzaSyBzl7rJtmxW8-BZwozv1KtBx-7cQ4S3p0g")
//                        .gceId("008012530695954790396:38ume8i2tfy").type(SearchSource.Type.GCE).build()));
//
//        log.info("gceCurationTest : {}"+ result);
//    }
//
//
//    @Test
//    public void gceCurationTestWithoutSource() {
//        Collection<Article> result = curationCrawler.startCurationCrawling(Arrays.asList("모니터링", "기술")
//                , null);
//
//        log.info("gceCurationTest : {}"+ result);
//    }
//
//}
