package com.secondspace.saffy.jpa;

import com.secondspace.saffy.dao.CurationKeywordDao;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.vo.model.CurationKeyword;
import com.secondspace.saffy.vo.model.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class JpaTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CurationKeywordDao curationKeywordDao;

    @Test
    public void testUser() {

        entityManager.persist(User.builder().uid("test").build());

        User user = userDao.getOne("test");

        assertEquals("test", user.getUid());
    }

    @Test
    public void testStream() {
        CurationKeyword c = entityManager.persist(CurationKeyword.builder().curationId(123).keyword("test").build());
        CurationKeyword c2 = entityManager.persist(CurationKeyword.builder().curationId(123).keyword("test2").build());
        entityManager.refresh(c);
        entityManager.refresh(c2);
        List<CurationKeyword> curationKeywordStream = curationKeywordDao.findAllByCurationId(123);

//        curationKeywordStream.

        assertNotNull(curationKeywordStream);
    }

}
