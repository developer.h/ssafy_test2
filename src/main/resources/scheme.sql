/*
SQLyog Community v13.1.0 (64 bit)
MySQL - 10.4.10-MariaDB-1:10.4.10+maria~bionic : Database - saffy_news
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`saffy_news` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `saffy_news`;

/*Table structure for table `article` */

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT 'scrap/skip',
  `is_posted` tinyint(4) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `title` varchar(1024) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `main_image` varchar(1024) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `article_idx_key_uid` (`uid`),
  KEY `article_idx_key_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Table structure for table `article_keyword` */

DROP TABLE IF EXISTS `article_keyword`;

CREATE TABLE `article_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `keyword` varchar(32) NOT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `article_keyword_idx_article_id_key` (`article_id`),
  KEY `article_keyword_idx_keyword_key` (`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_id` int(11) NOT NULL,
  `uid` char(8) NOT NULL,
  `target_uid` char(8) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `message` varchar(4096) NOT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `comment_idx_key_feed_id` (`feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `curation_keyword` */

DROP TABLE IF EXISTS `curation_keyword`;

CREATE TABLE `curation_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curation_id` int(11) NOT NULL,
  `keyword` varchar(32) NOT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `curation_keyword_idx_key_curation_id` (`curation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

/*Table structure for table `curation_set` */

DROP TABLE IF EXISTS `curation_set`;

CREATE TABLE `curation_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `curation_set_idx_key_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Table structure for table `curation_source` */

DROP TABLE IF EXISTS `curation_source`;

CREATE TABLE `curation_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curation_id` int(11) NOT NULL,
  `search_source_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `curation_source_idx_key_curation_id` (`curation_id`),
  KEY `curation_source_idx_key_search_source_id` (`search_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Table structure for table `feed` */

DROP TABLE IF EXISTS `feed`;

CREATE TABLE `feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `by_scrap` tinyint(4) DEFAULT NULL,
  `message` varchar(4096) DEFAULT NULL,
  `view_count` int(11) DEFAULT 0,
  `scrap_count` int(11) DEFAULT 0,
  `share_count` int(11) DEFAULT 0,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `feed_idx_key_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `feed_log` */

DROP TABLE IF EXISTS `feed_log`;

CREATE TABLE `feed_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_id` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT 'like/view/share',
  `create_date` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `follow` */

DROP TABLE IF EXISTS `follow`;

CREATE TABLE `follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `target_uid` char(8) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'requested/approved/denied',
  `approval_date` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `follow_uid_target_uid_index` (`uid`,`target_uid`),
  KEY `follow_uid_status_approval_date_index` (`uid`,`status`,`approval_date`),
  KEY `follow_target_uid_status_approval_date_index` (`target_uid`,`status`,`approval_date`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

/*Table structure for table `keyword_log` */

DROP TABLE IF EXISTS `keyword_log`;

CREATE TABLE `keyword_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(32) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT 'registration/search',
  `create_date` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `keyword_log_idx_key_keyword_type` (`keyword`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Table structure for table `likes` */

DROP TABLE IF EXISTS `likes`;

CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `target_uid` char(8) NOT NULL,
  `target_type` tinyint(4) NOT NULL COMMENT 'feed/comment',
  `target_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `like_idx_unique_uid_target_type_target_id` (`uid`,`target_type`,`target_id`),
  KEY `like_target_type_target_id_index` (`target_type`,`target_id`),
  KEY `like_idx_key_target_uid` (`target_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Table structure for table `noti_setting` */

DROP TABLE IF EXISTS `noti_setting`;

CREATE TABLE `noti_setting` (
  `uid` char(8) NOT NULL,
  `cancel_follower` tinyint(4) DEFAULT 1,
  `follower_new_feed` tinyint(4) DEFAULT 1,
  `my_feed_comment` tinyint(4) DEFAULT 1,
  `my_feed_like` tinyint(4) DEFAULT 1,
  `my_feed_scrap` tinyint(4) DEFAULT 1,
  `new_follower` tinyint(4) DEFAULT NULL,
  `new_follower_accept` tinyint(4) DEFAULT 1,
  `new_follower_accept_result` tinyint(4) DEFAULT 1,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `sender_uid` char(8) NOT NULL,
  `noti_type` tinyint(4) DEFAULT NULL,
  `data` varchar(1024) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `notification_idx_key_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Table structure for table `search_history` */

DROP TABLE IF EXISTS `search_history`;

CREATE TABLE `search_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `result_uid` char(8) DEFAULT NULL,
  `keyword` varchar(32) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

/*Table structure for table `search_source` */

DROP TABLE IF EXISTS `search_source`;

CREATE TABLE `search_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT 'rss/gce',
  `title` varchar(128) DEFAULT NULL,
  `rss_url` varchar(1024) DEFAULT NULL,
  `gce_id` varchar(128) DEFAULT NULL,
  `gce_api_key` varchar(128) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `search_source_idx_key_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `uid` char(8) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `nickname` varchar(32) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `profile_image` varchar(512) DEFAULT NULL,
  `content_type` varchar(32) DEFAULT NULL,
  `email_check` tinyint(4) DEFAULT 0,
  `deregi_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`uid`),
  UNIQUE KEY `user_idx_unique_email` (`email`),
  UNIQUE KEY `user_idx_unique_nick_name` (`nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_email_validation` */

DROP TABLE IF EXISTS `user_email_validation`;

CREATE TABLE `user_email_validation` (
  `code` varchar(64) NOT NULL,
  `uid` char(8) NOT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_privacy` */

DROP TABLE IF EXISTS `user_privacy`;

CREATE TABLE `user_privacy` (
  `uid` char(8) NOT NULL,
  `private_mode` tinyint(4) DEFAULT 0,
  `follow_approval` tinyint(4) DEFAULT 0,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_sns` */

DROP TABLE IF EXISTS `user_sns`;

CREATE TABLE `user_sns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` char(8) NOT NULL,
  `sns_type` tinyint(4) NOT NULL COMMENT 'naver/kakao/google',
  `sns_uid` varchar(128) DEFAULT NULL,
  `access_token` varchar(256) DEFAULT NULL,
  `create_date` datetime DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_sns_idx_key_uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
