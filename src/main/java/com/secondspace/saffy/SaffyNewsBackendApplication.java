package com.secondspace.saffy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaffyNewsBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaffyNewsBackendApplication.class, args);
    }

}
