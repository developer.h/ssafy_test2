package com.secondspace.saffy.controller;

import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.response.BaseResponse;
import com.secondspace.saffy.vo.response.RCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class SaffyExceptionHandler {
    @ExceptionHandler(SaffyException.class)
    public ResponseEntity<BaseResponse> handleSaffyException(SaffyException exception) {
        log.error("handleSaffyException", exception);
        BaseResponse errorResponse = new BaseResponse();

        errorResponse.setRCode(exception.getErrorCode().getResultCode());
        errorResponse.setRMsg(exception.getErrorMessage());

        return new ResponseEntity<>(errorResponse, exception.getErrorCode().getHttpStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<BaseResponse> handlerBindingException(MethodArgumentNotValidException e) {
        log.error("handlerBindingException", e);
        BaseResponse errorResponse = new BaseResponse();
        String[] codes = e.getBindingResult().getAllErrors().get(0).getCodes();
        String[] split = codes[0].split("\\.");
        String badParameter = split[split.length - 1];
        String reason = codes[3];

        if("NotNull".equals(reason)) {
            errorResponse.setRCode(RCode.BODY_REQUIRED.getResultCode());
            errorResponse.setRMsg(String.format(RCode.BODY_REQUIRED.getResultMessage(), badParameter));
        } else {
            errorResponse.setRCode(RCode.BODY_INVALID.getResultCode());
            errorResponse.setRMsg(String.format(RCode.BODY_INVALID.getResultMessage(), badParameter));
        }

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<BaseResponse> handlerMissingParameter(MissingServletRequestParameterException e) {
        log.error("handlerMissingParameter");

        BaseResponse errorResponse = new BaseResponse();
        String paramter = e.getParameterName();
        errorResponse.setRCode(RCode.PARAMETER_REQUIRED.getResultCode());
        errorResponse.setRMsg(String.format(RCode.PARAMETER_REQUIRED.getResultMessage(), paramter));
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseResponse> handleException(Exception exception) {
        log.error("handleException", exception);
        BaseResponse errorResponse = new BaseResponse();

        errorResponse.setRCode(RCode.INTERNAL_SERVER_ERROR.getResultCode());
        errorResponse.setRMsg(RCode.INTERNAL_SERVER_ERROR.getResultMessage());

        return new ResponseEntity<>(errorResponse, RCode.INTERNAL_SERVER_ERROR.getHttpStatus());
    }
}
