package com.secondspace.saffy.controller;

import com.secondspace.saffy.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    @Autowired
    private MailService mailService;

    @PostMapping("/api/v1/isValid")
    public void isValid(@RequestHeader(value = "Authorization") String Authorization) {

    }
    @GetMapping("/api/v1/mailTest")
    public void mailTest(@RequestParam String uid, @RequestParam String email) {
        mailService.sendMail(uid, email);
    }
}
