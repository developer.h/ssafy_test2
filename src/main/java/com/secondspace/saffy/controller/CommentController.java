package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.CommentService;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.service.NotificationService;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.feed.CommentRequestVO;
import com.secondspace.saffy.vo.feed.CommentVO;
import com.secondspace.saffy.vo.model.Notification;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.ListItemResponse;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class CommentController {

    private final CommentService commentService;
    private final NotificationService notificationService;
    private final FollowService followService;

    public CommentController(CommentService commentService, NotificationService notificationService, FollowService followService) {
        this.commentService = commentService;
        this.notificationService = notificationService;
        this.followService = followService;
    }


    @PostMapping("/comments")
    @ApiOperation(value = "댓글 등록")
    public ResponseEntity<SingleItemResponse<CommentVO>> createComment(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestBody CommentRequestVO request) {
        log.info("createFeed : " + request);

        CommentVO comment = commentService.createComment(user, request.getFeedId(),
                request.getParentCommentId(), request.getMessage());

        Map<String, Object> data = new HashMap<>();
        data.put("feedId", request.getFeedId());
        data.put("parentCommentId", request.getParentCommentId());

        notificationService.sendNotification(user, request.getOwnerUid(), Notification.Type.myFeedComment, data);

        return ResponseEntity.ok(SingleItemResponse.create(comment));
    }

    @GetMapping("/comments")
    @ApiOperation(value = "피드 댓글 조회")
    public ResponseEntity<ListItemResponse<CommentVO>> getAllCommentsByFeed(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUESTED_TARGET_USER, required = false) User targetUser,
            @RequestParam(value = ApiConstant.PARAM_TARGET_USER_ID, required = false) String targetUid,
            @RequestParam("feedId") final int feedId,
            PageableRequestParam pageParam) {
        log.info("getAllFeedsByFeed : " + targetUser + ", feedId : " + feedId + ", page : " + pageParam);
        followService.checkPermission(user.getUid(), targetUser.getUid());

        List<CommentVO> result = commentService.getAllCommentsByFeed(user, feedId, pageParam);
        return ResponseEntity.ok(ListItemResponse.create(checkILiked(user, result)));
    }

    @GetMapping("/comments/{commentId}")
    @ApiOperation(value = "피드 댓글 개별 조회")
    public ResponseEntity<SingleItemResponse<CommentVO>> getCommentById(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUESTED_TARGET_USER, required = false) User targetUser,
            @RequestParam(value = ApiConstant.PARAM_TARGET_USER_ID, required = false) String targetUid,
            @PathVariable("commentId") final int commentId) {
        log.info("getAllFeedsByFeed : " + targetUser + ", commentId : " + commentId);
        followService.checkPermission(user.getUid(), targetUser.getUid());

        CommentVO result = commentService.getCommentById(user, commentId);
        return ResponseEntity.ok(SingleItemResponse.create(checkILiked(user, result)));
    }

    @DeleteMapping("/comments/{commentId}")
    @ApiOperation(value = "댓글 삭제")
    public ResponseEntity<SingleItemResponse<EmptyJson>> deleteComment(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "commentId") int commentId) {
        log.info("deleteComment : " + commentId);

        commentService.deleteComment(user.getUid(), commentId);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    private List<CommentVO> checkILiked(User user, List<CommentVO> comments) {
        for (CommentVO comment : comments) {
            checkILiked(user, comment);
            for (CommentVO subComment : comment.getSubComments()) {
                checkILiked(user, subComment);
            }
        }
        return comments;
    }

    private CommentVO checkILiked(User user, CommentVO comment) {
        comment.getStatistics().setLiked(
                comment.getStatistics().getLikeUsers().stream().anyMatch(u -> u.getUid().equals(user.getUid())));
        return comment;
    }
}
