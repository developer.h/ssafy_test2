package com.secondspace.saffy.controller;

import com.secondspace.saffy.vo.EmptyJson;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.ArticleService;
import com.secondspace.saffy.vo.feed.ArticleRequestVO;
import com.secondspace.saffy.vo.feed.ArticleVO;
import com.secondspace.saffy.vo.model.Article;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.ListItemResponse;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import springfox.documentation.annotations.ApiIgnore;

import io.swagger.annotations.ApiOperation;

@Slf4j
@RestController
public class ArticleController {

    private final ArticleService articleService;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }


    @PostMapping("/articles")
    @ApiOperation(value = "Article 등록")
    public ResponseEntity<SingleItemResponse<ArticleVO>> createArticle(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestBody ArticleRequestVO request) {
        log.info("createArticle : " + request);

        return ResponseEntity.ok(
                SingleItemResponse.create(articleService.createArticle(user.getUid(), request))
        );
    }

    @GetMapping("/articles")
    @ApiOperation(value = "Article 조회")
    public ResponseEntity<ListItemResponse<ArticleVO>> getArticlesByStatus(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @RequestParam(ApiConstant.PARAM_ARTICLE_STATUS) Article.Status status,
            PageableRequestParam pageParam) {
        log.info("getArticlesByStatus : " + status + ", pageParam : " + pageParam);

        return ResponseEntity.ok(
                ListItemResponse.create(pageParam, articleService.getArticlesByStatus(user.getUid(), status, pageParam))
        );
    }


    @PutMapping("/articles/{articleId}")
    @ApiOperation(value = "Article status 변경")
    public ResponseEntity<SingleItemResponse<EmptyJson>> modifyArticleStatus(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "articleId") int articleId,
            @RequestParam(ApiConstant.PARAM_ARTICLE_STATUS) Article.Status status) {
        log.info("modifyArticleStatus : " + articleId);

        articleService.modifyArticleStatus(user.getUid(), articleId, status);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @DeleteMapping("/articles/{articleId}")
    @ApiOperation(value = "Article 삭제")
    public ResponseEntity<SingleItemResponse<EmptyJson>> deleteArticle(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "articleId") int articleId) {
        log.info("deleteArticle : " + articleId);

        articleService.deleteArticle(user.getUid(), articleId);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }
}
