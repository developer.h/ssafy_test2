package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.FeedService;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.service.NotificationService;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.feed.FeedRequestVO;
import com.secondspace.saffy.vo.feed.FeedScrapRequestVO;
import com.secondspace.saffy.vo.feed.FeedVO;
import com.secondspace.saffy.vo.follow.FollowVO;
import com.secondspace.saffy.vo.model.Notification;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.ListItemResponse;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class FeedController {

    private final FeedService feedService;
    private final FollowService followService;
    private final NotificationService notificationService;

    public FeedController(FeedService feedService, FollowService followService, NotificationService notificationService) {
        this.feedService = feedService;
        this.followService = followService;
        this.notificationService = notificationService;
    }

    @PostMapping("/feeds")
    @ApiOperation(value = "피드 등록")
    public ResponseEntity<SingleItemResponse<FeedVO>> createFeed(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestBody FeedRequestVO request) {
        log.info("createFeed : " + request);

        FeedVO feed = feedService.createFeed(user, request.getArticleId(), request.getMessage());

        List<FollowVO> followers = followService.getMyFollowers(user.getUid());

        Map<String, Integer> data = new HashMap<>();
        data.put("feedId", feed.getId());
        notificationService.sendNotification(user, followers, Notification.Type.followerNewFeed, data);

        return ResponseEntity.ok(SingleItemResponse.create(feed));
    }

    @PostMapping("/scrap/feeds")
    @ApiOperation(value = "피드 스크랩")
    public ResponseEntity<SingleItemResponse<FeedVO>> scrapFeed(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestBody FeedScrapRequestVO request) {
        log.info("scrapFeed : " + request);

        followService.checkPermission(user.getUid(), request.getOwnerUid());

        FeedVO feed = feedService.scrapFeed(user,
                request.getOwnerUid(), request.getFeedId(),
                request.getMessage());

        List<FollowVO> followers = followService.getMyFollowers(user.getUid());

        Map<String, Integer> data = new HashMap<>();
        data.put("feedId", feed.getId());
        notificationService.sendNotification(user, followers, Notification.Type.followerNewFeed, data);

        notificationService.sendNotification(user, request.getOwnerUid(), Notification.Type.myFeedScrap, data);

        return ResponseEntity.ok(SingleItemResponse.create(feed));
    }

    @GetMapping("/feeds")
    @ApiOperation(value = "피드 조회")
    public ResponseEntity<ListItemResponse<FeedVO>> getAllFeedsByUid(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(AppConstants.REQUESTED_TARGET_USER) User targetUser,
            @RequestParam(value = ApiConstant.PARAM_TARGET_USER_ID, required = false) String targetUid,
            @RequestParam(name = "sort", required = false) final ApiConstant.FeedSortParam sort,
            PageableRequestParam pageParam) {
        log.info("getAllFeedsByUid : " + targetUser + ", sort : " + sort + ", page : " + pageParam);
        followService.checkPermission(user.getUid(), targetUser.getUid());

        List<FeedVO> result = feedService.getAllFeedsByUid(targetUser);
        if (sort != null) {
            if (sort == ApiConstant.FeedSortParam.COMMENT)
                result.sort((f1, f2) -> f1.getStatistics().getCommentCount() < f2.getStatistics().getCommentCount() ? 1 : -1);
            else if (sort == ApiConstant.FeedSortParam.LIKE)
                result.sort((f1, f2) -> f1.getStatistics().getLikeCount() < f2.getStatistics().getLikeCount() ? 1 : -1);
            else if (sort == ApiConstant.FeedSortParam.SCRAP)
                result.sort((f1, f2) -> f1.getStatistics().getScrapCount() < f2.getStatistics().getScrapCount() ? 1 : -1);
            ;
        }

        result = checkILiked(user, result);
        int start = Integer.parseInt(pageParam.getPageStartToken(0));
        int end = Integer.min(start + pageParam.getCount(), result.size());
        ListItemResponse response = ListItemResponse.create(result.subList(start, end));
        if (end < result.size()) {
            response.setHasMore(true);
            response.setNextPageToken(end + "");
        }

        return ResponseEntity.ok(response);
    }

    @GetMapping("/feeds/{feedId}")
    @ApiOperation(value = "피드 개별 조회")
    public ResponseEntity<SingleItemResponse<FeedVO>> getFeedsByUid(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(AppConstants.REQUESTED_TARGET_USER) User targetUser,
            @RequestParam(value = ApiConstant.PARAM_TARGET_USER_ID, required = false) String targetUid,
            @PathVariable("feedId") int feedId) {
        log.info("getFeedsByUid : " + targetUser + ", feedId : " + feedId);
        followService.checkPermission(user.getUid(), targetUser.getUid());

        FeedVO feed = feedService.getFeed(targetUser, feedId);

        return ResponseEntity.ok(SingleItemResponse.create(checkILiked(targetUser, feed)));
    }

    @GetMapping("/search-feeds/like")
    @ApiOperation(value = "좋아요 한 피드 조회")
    public ResponseEntity<ListItemResponse<FeedVO>> getLikeFeeds(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(AppConstants.REQUESTED_TARGET_USER) User targetUser,
            @RequestParam(ApiConstant.PARAM_TARGET_USER_ID) String uid,
            PageableRequestParam pageParam) {
        log.info("getLikeFeeds : " + targetUser + ", page : " + pageParam);
        followService.checkPermission(user.getUid(), targetUser.getUid());


        List<FeedVO> result = feedService.findUserLikedFeeds(targetUser, pageParam);
        return ResponseEntity.ok(ListItemResponse.create(checkILiked(user, result)));
    }

    @GetMapping("/search-feeds/keyword")
    @ApiOperation(value = "키워드로 피드 검색")
    public ResponseEntity<ListItemResponse<FeedVO>> searchFeedsByKeyword(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @RequestParam(ApiConstant.PARAM_SEARCH_KEYWORD) String keyword,
            PageableRequestParam pageParam) {
        log.info("searchFeedsByKeyword : " + keyword + ", page : " + pageParam);
        List<FeedVO> result = feedService.searchFeedsByKeyword(user, keyword, pageParam);
        return ResponseEntity.ok(ListItemResponse.create(pageParam, checkILiked(user, result)));
    }

    @DeleteMapping("/feeds/{feedId}")
    @ApiOperation(value = "피드 삭제")
    public ResponseEntity<SingleItemResponse<EmptyJson>> deleteFeed(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "feedId") int feedId) {
        log.info("deleteFeed : " + feedId);

        feedService.deleteFeed(user.getUid(), feedId);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    private List<FeedVO> checkILiked(User user, List<FeedVO> feeds) {
        for (FeedVO feed : feeds) {
            checkILiked(user, feed);
        }
        return feeds;
    }

    private FeedVO checkILiked(User user, FeedVO feed) {
        feed.getStatistics().setLiked(
                feed.getStatistics().getLikeUsers().stream().anyMatch(u -> u.getUid().equals(user.getUid())));
        return feed;
    }
}
