package com.secondspace.saffy.controller;

import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.service.UserService;
import com.secondspace.saffy.util.UidUtil;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.follow.FollowVO;
import com.secondspace.saffy.vo.follow.FollowerApproveRequest;
import com.secondspace.saffy.vo.response.ListItemResponse;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

@RestController
@Slf4j
public class FollowerController {
    @Autowired
    FollowService followService;
    @Autowired
    UserService userService;

    @GetMapping("/followers")
    @ApiOperation(value = "나의 팔로워 목록을 조회합니다")
    public ResponseEntity<ListItemResponse<FollowVO>> getFollowers(
            @RequestHeader(value = "Authorization", required = true) String authrization,
            @RequestParam(value = "uid", required = false) String uid,
            PageableRequestParam pageParam) {
        String caller = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("getFollowers: caller="+caller+" ,target = ", uid+", page="+pageParam.toString());

        String targetUserId = uid;

        if(uid == null)
            targetUserId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());

        log.info("getFollowers: caller="+caller+", target="+uid);

        if(targetUserId.equals(caller))
            return ResponseEntity.ok(ListItemResponse.create(pageParam, followService.getMyFollowers(targetUserId, pageParam)));
        else
            return ResponseEntity.ok(ListItemResponse.create(pageParam, followService.getUserFollowers(caller, targetUserId, pageParam)));
    }

    @PutMapping("followers/approval")
    @ApiOperation(notes = "팔로워 요청을 승인합니다.", value = "/followers/approval")
    public ResponseEntity<SingleItemResponse<EmptyJson>> followApproval(
            @RequestHeader(value = "Authorization", required = true) String authrization,
            @RequestBody FollowerApproveRequest approvalRequest) throws SaffyException {
        String userId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("followApproval: caller="+userId+", request="+approvalRequest.toString());
        followService.approve(userId, approvalRequest);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @GetMapping("/followers/waitApproval")
    @ApiOperation(notes = "승인 대기 목록을 조회합니다", value = "/follwers/waitApproval")
    @ApiResponse(code = 200, message = "Success", response = ListItemResponse.class)
    public ResponseEntity<ListItemResponse<FollowVO>> getWaitForApprovalList (
            @RequestHeader(value = "Authorization", required = true) String authrization, PageableRequestParam pageableRequestParam) throws SaffyException {
        String uid = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("getWaitForApprovalList: caller="+uid+", page="+pageableRequestParam.toString());
        return ResponseEntity.ok(ListItemResponse.create(pageableRequestParam, followService.getWaitForApprovals(uid, pageableRequestParam)));
    }

}
