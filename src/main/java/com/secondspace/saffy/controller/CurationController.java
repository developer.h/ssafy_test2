package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.CurationService;
import com.secondspace.saffy.service.SearchSourceService;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.curation.*;
import com.secondspace.saffy.vo.feed.ArticleCurationVO;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.ListItemResponse;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Slf4j
@RestController
public class CurationController {

    private final CurationService curationService;

    private final SearchSourceService searchSourceService;

    public CurationController(CurationService curationService, SearchSourceService searchSourceService) {
        this.curationService = curationService;
        this.searchSourceService = searchSourceService;
    }

    @GetMapping("/crawl/curation-set")
    @ApiOperation(value = "큐레이션세트 실행")
    public ResponseEntity<ListItemResponse<ArticleCurationVO>> crawlCurationSet(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestParam(name = "curationSetIds") Integer[] curationSetIds) {
        log.info("crawlCurationSet : " + curationSetIds);

        List<ArticleCurationVO> articles = curationService.crawlCuration(user.getUid(), curationSetIds);

        return ResponseEntity.ok(ListItemResponse.create(articles));
    }

    @GetMapping("/curation-set")
    @ApiOperation(value = "큐레이션세트 조회")
    public ResponseEntity<ListItemResponse<CurationSetVO>> getAllCurationSetWithDetails(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUESTED_TARGET_USER, required = false) User targetUser,
            @RequestParam(value = ApiConstant.PARAM_TARGET_USER_ID, required = false) String targetUid,
            PageableRequestParam pageParam) {
        log.info("getAllCurationSetWithDetails : " + targetUser + ", page : " + pageParam);

        return ResponseEntity.ok(
                ListItemResponse.create(pageParam, curationService.getAllCurationSetWithDetails(targetUser.getUid(), pageParam))
        );
    }

    @GetMapping("/curation-set/{curationSetId}")
    @ApiOperation(value = "큐레이션세트 개별 조회")
    public ResponseEntity<SingleItemResponse<CurationSetVO>> getCurationSetWithDetails(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUESTED_TARGET_USER, required = false) User targetUser,
            @RequestParam(value = ApiConstant.PARAM_TARGET_USER_ID, required = false) String targetUid,
            @PathVariable("curationSetId") int curationSetId
    ) {
        log.info("getCurationSetWithDetails : " + targetUser + ", curation : " + curationSetId);

        return ResponseEntity.ok(
                SingleItemResponse.create(curationService.getCurationSetWithDetails(targetUser.getUid(), curationSetId))
        );
    }

    @PostMapping("/curation-set")
    @ApiOperation(value = "큐레이션세트 등록")
    public ResponseEntity<SingleItemResponse<EmptyJson>> createCurationSet(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestBody CurationSetCreateRequestVO request) {
        log.info("createCurationSet : " + request);

        curationService.createCurationSet(user.getUid(), request.getName(), request.getKeywords(), request.getSearchSources());
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @PutMapping("/curation-set/{curationSetId}")
    @ApiOperation(value = "큐레이션세트 수정")
    public ResponseEntity<SingleItemResponse<EmptyJson>> editCurationSet(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "curationSetId") int curationSetId,
            @RequestBody CurationSetUpdateRequestVO request) {
        log.info("editCurationSet : " + curationSetId + ", " + request);

        curationService.editCurationSet(user.getUid(), null, curationSetId, request.getName(),
                request.getAddedKeywords(), request.getDeletedKeywords(),
                request.getAddedSearchSources(), request.getDeletedSearchSources());
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @PutMapping("/scrap/curation-set/{curationSetId}")
    @ApiOperation(value = "큐레이션세트 가져오기")
    public ResponseEntity<SingleItemResponse<EmptyJson>> scrapCurationSet(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "curationSetId") int curationSetId,
            @RequestBody CurationSetScrapRequestVO request) {
        log.info("scrapCurationSet : " + request);

        curationService.editCurationSet(user.getUid(), request.getSourceUid(), curationSetId, null,
                request.getAddedKeywords(), null,
                request.getAddedSearchSources(), null);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @DeleteMapping("/curation-set/{curationSetId}")
    @ApiOperation(value = "큐레이션세트 삭제")
    public ResponseEntity<SingleItemResponse<EmptyJson>> deleteCurationSet(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "curationSetId") int curationSetId) {
        log.info("deleteCurationSet : " + curationSetId);

        curationService.deleteCurationSet(user.getUid(), curationSetId);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }


    @GetMapping("/search-sources")
    @ApiOperation(value = "고급검색 조회")
    public ResponseEntity<ListItemResponse<SearchSourceVO>> getAllSearchSources(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            PageableRequestParam pageParam) {
        log.info("getAllSearchSources : " + pageParam);

        return ResponseEntity.ok(
                ListItemResponse.create(pageParam, searchSourceService.getAllSearchSources(user.getUid(), pageParam))
        );
    }

    @GetMapping("/search-sources/{searchSourceId}")
    @ApiOperation(value = "고급검색 개별 조회")
    public ResponseEntity<SingleItemResponse<SearchSourceVO>> getSearchSource(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "searchSourceId") int searchSourceId) {
        log.info("getSearchSource : " + searchSourceId);

        return ResponseEntity.ok(
                SingleItemResponse.create(searchSourceService.getSearchSource(user.getUid(), searchSourceId))
        );
    }

    @PostMapping("/search-sources")
    @ApiOperation(value = "고급검색 저장")
    public ResponseEntity<SingleItemResponse<EmptyJson>> createSearchSource(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestBody SearchSourceCreateRequestVO request) {
        log.info("createSearchSource : " + request);

        searchSourceService.createSearchSource(user.getUid(), request.getTitle(), request.getType(), request.getRssUrl()
                , request.getGceId(), request.getGceApiKey());
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @PutMapping("/search-sources/{searchSourceId}")
    @ApiOperation(value = "고급검색 수정")
    public ResponseEntity<SingleItemResponse<EmptyJson>> updateSearchSource(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "searchSourceId") int searchSourceId,
            @RequestBody SearchSourceVO request) {
        log.info("createSearchSource : " + request);

        searchSourceService.updateSearchSource(user.getUid(), searchSourceId, request.getTitle(), request.getType(), request.getRssUrl()
                , request.getGceId(), request.getGceApiKey());
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @DeleteMapping("/search-sources/{searchSourceId}")
    @ApiOperation(value = "고급검색 삭제")
    public ResponseEntity<SingleItemResponse<EmptyJson>> deleteSearchSource(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @PathVariable(name = "searchSourceId") int searchSourceId) {
        log.info("deleteSearchSource : " + searchSourceId);

        searchSourceService.deleteSearchSource(user.getUid(), searchSourceId);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

}
