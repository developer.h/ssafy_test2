package com.secondspace.saffy.controller;

import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.LikeService;
import com.secondspace.saffy.service.NotificationService;
import com.secondspace.saffy.vo.model.Likes;
import com.secondspace.saffy.vo.model.Notification;
import com.secondspace.saffy.vo.model.User;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiOperation;

@Slf4j
@RestController
public class LikeController {

    private final LikeService likeService;
    private final NotificationService notificationService;
    private final FollowService followService;

    public LikeController(LikeService likeService, NotificationService notificationService, FollowService followService) {
        this.likeService = likeService;
        this.notificationService = notificationService;
        this.followService = followService;
    }


    @PostMapping("/like")
    @ApiOperation(value = "좋아요 실행/취소")
    public ResponseEntity<SingleItemResponse<EmptyJson>> like(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            @RequestParam(ApiConstant.PARAM_TARGET_USER_ID) String targetUid,
            @RequestParam(ApiConstant.PARAM_LIKE_TARGET_TYPE) Likes.TargetType targetType,
            @RequestParam(ApiConstant.PARAM_LIKE_TARGET_ID) int targetId,
            @RequestParam(ApiConstant.PARAM_LIKE_ONOFF) boolean liked
    ) {
        log.info("like : " + targetType + ", id : " + targetId + ", like : " + liked);

        followService.checkPermission(user.getUid(), targetUid);

        likeService.setLike(user.getUid(), targetUid, targetType, targetId, liked);

        Map<String, Object> data = new HashMap<>();
        data.put("targetId", targetId);
        data.put("targetType", targetType);
        notificationService.sendNotification(user, targetUid, Notification.Type.myFeedLike, data);

        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

}
