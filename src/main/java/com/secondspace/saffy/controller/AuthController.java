package com.secondspace.saffy.controller;

import com.secondspace.saffy.auth.JwtToken;
import com.secondspace.saffy.auth.JwtUserDetailsService;
import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.exception.ErrorCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.service.UserDatabaseService;
import com.secondspace.saffy.vo.auth.AuthenticationRequest;
import com.secondspace.saffy.vo.auth.JwtTokenResponse;

import com.secondspace.saffy.vo.db.UserTable;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtToken jwtToken;
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private UserDatabaseService userDatabaseService;


    @PostMapping("/api/v1/account/authenticate")
    @ApiOperation(notes = "test API", value = "/api/v1/account/authenticate")
    public ResponseEntity<JwtTokenResponse> signIn(
            @Valid @RequestBody AuthenticationRequest authRequest) throws SaffyException {

        validateAuthenticationRequest(authRequest);

        UserTable userTable = null;
        if("".equals(authRequest.getSnsType())) {
            userTable = idPasswordAuthenticate(authRequest.getEmail(), authRequest.getPassword());
        } else {
            userTable = socialAuthenticate(authRequest.getSnsUid(), authRequest.getEmail(), authRequest.getName(), authRequest.getSnsType());
        }

        final String token = jwtToken.generateToken(userTable);

        return ResponseEntity.ok(new JwtTokenResponse(token));

    }

    private UserTable socialAuthenticate(String snsUid, String email, String name, String snsType) throws SaffyException {
        UserTable userTable = userDatabaseService.getUserTableByUid(snsUid);
        if(userTable != null)
            return userTable;

        //기존 사용자가 없으므로 회원가입을 위해 데이터베이스에 추가
        userTable = new UserTable();
        userTable.setEmail(email);
        userTable.setSocial_type(snsType);
        userTable.setUid(snsUid);

        userDatabaseService.addUser(userTable);

        return userTable;
    }

    private void validateAuthenticationRequest(AuthenticationRequest authRequest) throws SaffyException {
        String nullParameter = null;
        if("".equals(authRequest.getSnsType())) {
            if(authRequest.getEmail() == null)
                nullParameter = ApiConstant.paramEmail;
            else if(authRequest.getPassword() == null)
                nullParameter = ApiConstant.paramPassword;
            if(nullParameter != null) {
                throw new SaffyException(ErrorCode.PARAMETER_REQUIRED, nullParameter, null);
            }
        } else {
            if(authRequest.getSnsUid() == null)
                nullParameter = ApiConstant.paramSnsUid;

            if(nullParameter != null) {
                throw new SaffyException(ErrorCode.PARAMETER_REQUIRED, nullParameter, null);
            }
        }
    }

    private UserTable idPasswordAuthenticate(String userName, String password) throws SaffyException {
        UserTable userTable = userDatabaseService.getUserTableByEmail(userName);

        if(userTable == null) {
            throw new SaffyException(ErrorCode.UNAUTHORIZED);
        }

        if(userTable.getPassword() == null && !userTable.getPassword().equals(password)) {
            throw new SaffyException(ErrorCode.UNAUTHORIZED);
        }

        return userTable;
    }
}