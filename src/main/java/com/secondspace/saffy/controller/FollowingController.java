package com.secondspace.saffy.controller;

import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.follow.FollowVO;
import com.secondspace.saffy.vo.response.*;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.service.UserService;
import com.secondspace.saffy.util.UidUtil;
import com.secondspace.saffy.vo.model.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

@RestController
@Slf4j
public class FollowingController {
    @Autowired
    FollowService followService;
    @Autowired
    UserService userService;

    @GetMapping("/followings")
    @ApiOperation(notes = "팔로잉 목록을 조회합니다", value = "/followers")
    @ApiResponse(code = 200, message = "Success", response = ListItemResponse.class)
    public ResponseEntity<ListItemResponse<FollowVO>> getFollowings(
            @RequestHeader(value = "Authorization", required = true) String authrization,
            @RequestParam(value = "uid", required = false) String uid,
            PageableRequestParam pageParam) throws SaffyException {
        String caller = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("getFollowings: caller="+caller+"target="+uid+", page="+pageParam.toString());
        String targetUserId = uid;

        if(targetUserId == null)
            targetUserId = caller;

        if(targetUserId.equals(caller))
            return ResponseEntity.ok(ListItemResponse.create(pageParam, followService.getMyFollowings(targetUserId, pageParam)));
        else
            return ResponseEntity.ok(ListItemResponse.create(pageParam, followService.getUserFollowings(caller, targetUserId, pageParam)));
    }

    @PostMapping("/followings")
    @ApiOperation(notes = "상대팡을 팔로우 합니다", value = "/followings")
    public ResponseEntity<SingleItemResponse<EmptyJson>> follow(
            @RequestHeader(value = "Authorization", required = true) String authorization,
            @RequestParam(value = "uid") String uid) throws SaffyException {
        String callerUid = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("follow: caller="+callerUid+", target="+uid);
        if(uid != null) {
            User user = userService.getUser(uid);
            if (user == null) {
                throw new SaffyException(RCode.CANNOT_FIND_USER_FOR_FOLLOWING, uid);
            }

            if(uid.equals(callerUid)) {
                throw new SaffyException(RCode.CANNOT_FOLLOW_YOURSELF);
            }
        }

        followService.checkAlreadyFollowing(callerUid, uid);
        followService.follow(callerUid, uid);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @DeleteMapping("/followings")
    @ApiOperation(notes = "팔로우를 취소합니다", value = "/followings")
    public ResponseEntity<SingleItemResponse<EmptyJson>> cancelFollow(
            @RequestHeader(value = "Authorization", required = true) String authorization,
            @RequestParam(value = "uid") String uid) throws SaffyException {
        String callerUid = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("cancelFollow: caller="+callerUid+", target="+uid);
        followService.cancelFollow(callerUid, uid);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }
}
