package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.service.NotificationService;
import com.secondspace.saffy.service.UserService;
import com.secondspace.saffy.util.UidUtil;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.model.UserSns;
import com.secondspace.saffy.vo.response.*;
import com.secondspace.saffy.vo.user.*;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@RestController
public class UserProfileController {
    @Autowired
    UserService userService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    FollowService followService;

    @Value("${profilePath}")
    private String profilePath;

    private String getSnsType(UserSns.SnsType type) {

        if (type == UserSns.SnsType.GOOGLE) {
            return ApiConstant.socialTypeGoogle;
        } else if (type == UserSns.SnsType.KAKAO) {
            return ApiConstant.socialTypeKAKAO;
        } else {
            return ApiConstant.socialTypeNaver;
        }
    }

    @GetMapping("/user/{userId}")
    @ApiOperation(notes = "사용자의 프로필 정보를 제공합니다", value = "/user/{userId}")
    @ApiResponse(code = 200, message = "Success", response = ProfileVO.class)
    public ResponseEntity<SingleItemResponse<ProfileVO>> getUserProfile(
            @RequestHeader(value = "Authorization", required = true) String authorization,
            @PathVariable( value = "userId") String userId) throws SaffyException {
        String callerUserId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("getUserProfile: caller="+callerUserId+", target="+userId);
        User user = userService.getUser(userId);

        if (user == null) {
            throw new SaffyException(RCode.CANNOT_FIND_USER_FOR_PROFILE, userId);
        }

        if(callerUserId.equals(userId))
            return ResponseEntity.ok(SingleItemResponse.create(userService.getMyProfile(userId)));

        return ResponseEntity.ok(SingleItemResponse.create(userService.getUserProfile(callerUserId, userId)));
    }

    @GetMapping("/user/myProfile")
    @ApiOperation(notes = "사용자의 프로필 정보를 제공합니다", value = "/user/myProfile")
    @ApiResponse(code = 200, message = "Success", response = SingleItemResponse.class)
    public ResponseEntity<SingleItemResponse<ProfileVO>> getMyProfile(
            @RequestHeader(value = "Authorization", required = true) String authorization) throws SaffyException {
        String userId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("getUserProfile: caller="+userId);
        return ResponseEntity.ok(SingleItemResponse.create(userService.getMyProfile(userId)));
    }

    @PutMapping(value = "/user/myProfile")
    @ApiOperation(notes = "사용자의 프로필 정보를 수정 합니다", value = "/user/myProfile")
    @ApiModelProperty
    public ResponseEntity<SingleItemResponse<EmptyJson>> changeMyProfile(@RequestPart(value = "image", required = false) MultipartFile file,
                                                                         @RequestPart(value = "profile", required = false) UserProfileRequestVO profileRequestVO,
                                                                         @RequestHeader(value = "Authorization", required = true) String authorization) throws SaffyException {
        String userId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        String profileRequestStr = profileRequestVO == null ? null : profileRequestVO.toString();
        log.info("changeMyProfile: caller"+userId+", request= "+profileRequestStr);
        String nickname = null;
        String description = null;
        String fileName = null;
        String contentsType = null;
        if (profileRequestVO != null) {
            nickname = profileRequestVO.getNickname();
            description = profileRequestVO.getDescription();


            User user = userService.getUserByNickname(nickname);

            if (user != null && !user.getUid().equals(userId)) {
                throw new SaffyException(RCode.NICKNAME_ALREADY_IN_USE, nickname);
            }
        }
        if (file != null) {
            String originFileName = file.getOriginalFilename();
            String fileExtension = originFileName.contains(".") ?
                    "." + originFileName.substring(originFileName.lastIndexOf(".") + 1) : null;
            fileName = userId + "_" + System.currentTimeMillis() + fileExtension;
            contentsType = file.getContentType();

            Path fileLocation = Paths.get(profilePath + fileName).toAbsolutePath().normalize();
            try {
                file.transferTo(fileLocation);
            } catch (IOException e) {

            }
        }


        userService.updateUser(userId, nickname, description, fileName, contentsType);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @GetMapping("/user/profileImage")
    @ApiOperation(notes = "사용자의 프로필 이미지를 다운로드 합니다", value = "/user/profileImage")
    @ApiModelProperty
    public ResponseEntity<Resource> getMyProfileImage(
            @RequestParam(value = "userId", required = false) String userId) throws SaffyException {
        User user = userService.getUser(userId);

        if (user == null || user.getProfileImage() == null)
            return ResponseEntity.ok(null);

        File profileFile = new File(profilePath + user.getProfileImage());
        InputStream is = null;
        try {
            is = new FileInputStream(profileFile);
        } catch (FileNotFoundException e) {
            return null;
        }

        return ResponseEntity.ok()
                .contentType(MediaType
                        .parseMediaType(user.getContentType()))
                .body(new InputStreamResource(is));
    }

    @GetMapping("/user/search")
    @ApiOperation(notes = "사용자를 검색합니다", value = "/user/search")
    @ApiModelProperty
    public ResponseEntity<SingleItemResponse<ProfileVO>> searchUser(
            @RequestHeader(value = "Authorization") String authorization,
            @RequestParam(value = "nickname") String nickname) throws SaffyException {
        User user = userService.getUserByNickname(nickname);

        if (user == null) {
            return ResponseEntity.ok(SingleItemResponse.create(null));
        }

        return ResponseEntity.ok(SingleItemResponse.create(ProfileVO.from(user)));
    }

    @PutMapping("/user/settings/privacy")
    @ApiOperation(notes = "사용자의 공개 정책을 설정합니다", value = "/user/settings/privacy")
    @ApiModelProperty
    public ResponseEntity<SingleItemResponse<EmptyJson>> setUserPrivacy(@RequestHeader(value = "Authorization", required = true) String authorization,
                               @RequestBody PrivacySettingVO privacySettingVO) throws SaffyException {
        String userId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("setUserPrivacy: caller="+userId+", request="+privacySettingVO.toString());
        if (privacySettingVO.getIsFollowAccept() == null && privacySettingVO.getIsPublic() == null) {
            throw new SaffyException(RCode.BODY_REQUIRED, ApiConstant.IS_FOLLOW_ACCEPT);
        }

        userService.updateUserPrivacy(userId, privacySettingVO.getIsPublic(), privacySettingVO.getIsFollowAccept());
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @PutMapping("/user/settings/notification")
    @ApiOperation(notes = "사용자의 알림을 설정합니다", value = "/user/settings/notification")
    @ApiModelProperty
    public ResponseEntity<SingleItemResponse<EmptyJson>> updateNotification(@RequestHeader(value = "Authorization", required = true) String authorization,
                                   @RequestBody NotificationSettingVO notificationSettingVO) throws SaffyException {
        String userId = UidUtil.getUidFromContext(RequestContextHolder.getRequestAttributes());
        log.info("updateNotification: caller="+userId+", request="+notificationSettingVO.toString());
        userService.updateNotiSettigs(userId, notificationSettingVO);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }


    @GetMapping("/user/notifications")
    @ApiOperation(value = "알림 조회")
    public ResponseEntity<ListItemResponse<NotificationVO>> getNotifications(
            @ApiIgnore @RequestAttribute(name = AppConstants.REQUEST_USER) User user,
            PageableRequestParam pageParam) {
        log.info("getNotifications - page : " + pageParam);

        return ResponseEntity.ok(ListItemResponse.create(pageParam, notificationService.getNotifications(user.getUid(), pageParam)));
    }

}
