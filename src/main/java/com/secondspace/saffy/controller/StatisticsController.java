package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.FeedService;
import com.secondspace.saffy.service.FollowService;
import com.secondspace.saffy.service.StatisticsService;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.feed.FeedVO;
import com.secondspace.saffy.vo.model.FeedLog;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.ListItemResponse;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import com.secondspace.saffy.vo.user.UserStatisticsVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
public class StatisticsController {

    private final StatisticsService statisticsService;
    private final FeedService feedService;
    private final FollowService followService;

    public StatisticsController(StatisticsService statisticsService, FeedService feedService, FollowService followService) {
        this.statisticsService = statisticsService;
        this.feedService = feedService;
        this.followService = followService;
    }


    @GetMapping("/stats/users/{uid}")
    @ApiOperation(value = "유저 통계 조회")
    public ResponseEntity<SingleItemResponse<UserStatisticsVO>> getUserStatistics(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @PathVariable("uid") String targetUid,
            @RequestHeader(value = "Authorization", required = true)String token) {
        log.info("getUserStatistics : " + targetUid);
        followService.checkPermission(user.getUid(), targetUid);

        return ResponseEntity.ok(SingleItemResponse.create(statisticsService.getUserStatistics(targetUid)));
    }

    @GetMapping("/stats/top/feeds")
    @ApiOperation(value = "인기 피드 조회")
    public ResponseEntity<ListItemResponse<FeedVO>> getTopFeeds(
            PageableRequestParam pageParam
    ) {
        log.info("getTopFeeds : " + pageParam);

        return ResponseEntity.ok(ListItemResponse.create(pageParam, feedService.getTopFeeds(pageParam)));
    }

    @GetMapping("/stats/top/keywords")
    @ApiOperation(value = "인기 키워드 조회")
    public ResponseEntity<ListItemResponse<String>> getTopKeywords(
            PageableRequestParam pageParam
    ) {
        log.info("getTopKeywords : " + pageParam);

        return ResponseEntity.ok(ListItemResponse.create(pageParam, statisticsService.getTopKeywords(pageParam)));
    }

    @PutMapping("/stats/count")
    @ApiOperation(value = "피드 통계 Count 업데이트(View, 스크랩, 공유)")
    public ResponseEntity<SingleItemResponse<EmptyJson>> updateFeedStatisticsCount(
            @RequestParam("feedId") int feedId,
            @RequestParam("type") FeedLog.Type type) {
        log.info("updateStatisticsCount : " + feedId + ", type : " + type);

        statisticsService.updateFeedStatisticsCount(feedId, type);

        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }


}
