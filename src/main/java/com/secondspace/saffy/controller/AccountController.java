package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.service.MailService;
import com.secondspace.saffy.vo.auth.*;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.service.AuthenticationService;
import com.secondspace.saffy.service.UserService;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.BaseResponse;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
public class AccountController {
    @Autowired
    private UserService userService;
    @Autowired
    AuthenticationService authenticationService;
    @Autowired
    MailService mailService;

    @PostMapping("/account/signup")
    @ApiOperation(value = "회원가입")
    public ResponseEntity<SingleItemResponse<EmptyJson>> signUp(@Valid @RequestBody SignUpRequest request) throws SaffyException {
        log.info("signUp: "+request.toString());
        userService.signUpWithEmail(request.getEmail(), request.getPassword(), request.getNickname());
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @PostMapping("/account/login")
    @ApiOperation(value = "토큰 발급을 요청합니다")
    public ResponseEntity<SingleItemResponse<JwtTokenResponse>> authenticate(
            @Valid @RequestBody AuthenticationRequest authRequest) throws SaffyException {
        log.info("authenticate: "+authRequest.toString());
        validateAuthenticationRequest(authRequest);

        String token = "aaa";
        User user = null;
        if ("".equals(authRequest.getSnsType())) {
            token = authenticationService.emailPasswordAuthenticate(authRequest.getEmail(), authRequest.getPassword());

        } else {
            token = authenticationService.socialAuthenticate(authRequest.getSnsUid(), authRequest.getEmail(), authRequest.getSnsType());
        }
        JwtTokenResponse tokenResponse = new JwtTokenResponse(token);
        return ResponseEntity.ok(SingleItemResponse.create(tokenResponse));
    }

    @PutMapping("/account/password/changeRequest")
    @ApiOperation(value = "비밀번호 변경을 요청합니다")
    public ResponseEntity<SingleItemResponse<EmptyJson>> changePasswordRequest(@RequestBody PasswordChangeRequestRequest password) throws SaffyException {
        log.info("changePasswordRequest : "+ password.toString());
        if(password.getEmail() == null && password.getUserId() == null) {
            throw new SaffyException(RCode.BODY_REQUIRED, "userId");
        }

        userService.changePasswordRequest(password);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    @PutMapping("/account/password")
    @ApiOperation(value = "비밀번호를 변경합니다")
    public ResponseEntity<SingleItemResponse<EmptyJson>>  changePassword(@Valid @RequestBody PasswordChangeRequest password) throws SaffyException {
        log.info("changePassword :"+ password.toString());
        userService.changePassword(password);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }


    @GetMapping("/account/validate")
    @ApiOperation(value = "이메일 인증")
    public ResponseEntity<SingleItemResponse<EmptyJson>> validate(@Valid @RequestParam(value = "authKey")String code) throws SaffyException {
        log.info("validate :"+ code);
        userService.validateCode(code);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }

    private void validateAuthenticationRequest(AuthenticationRequest authRequest) throws SaffyException {
        String nullParameter = null;
        if ("".equals(authRequest.getSnsType())) {
            if (authRequest.getEmail() == null)
                nullParameter = ApiConstant.paramEmail;
            else if (authRequest.getPassword() == null)
                nullParameter = ApiConstant.paramPassword;
            if (nullParameter != null) {
                throw new SaffyException(RCode.PARAMETER_REQUIRED, nullParameter, null);
            }
        } else {
            if (authRequest.getSnsUid() == null)
                nullParameter = ApiConstant.paramSnsUid;

            if (nullParameter != null) {
                throw new SaffyException(RCode.PARAMETER_REQUIRED, nullParameter, null);
            }
        }
    }
}