package com.secondspace.saffy.controller;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.SearchService;
import com.secondspace.saffy.vo.EmptyJson;
import com.secondspace.saffy.vo.model.SearchHistory;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.SingleItemResponse;
import com.secondspace.saffy.vo.search.SearchResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;

@Slf4j
@RestController
public class SearchController {

    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping("/search")
    @ApiOperation(value = "검색 api")
    public ResponseEntity<SingleItemResponse<SearchResultVO>> search(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @RequestParam(ApiConstant.PARAM_SEARCH_QUERY) @NotEmpty String query) {
        log.info("search : " + query);

        return ResponseEntity.ok(SingleItemResponse.create(searchService.searchByKeyword(user.getUid(), query)));
    }

    @GetMapping("/search-history")
    @ApiOperation(value = "검색 히스토리 조회 api")
    public ResponseEntity<SingleItemResponse<SearchResultVO>> getSearchHistory(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user) {
        log.info("getSearchHistory : ");

        return ResponseEntity.ok(SingleItemResponse.create(searchService.getSearchHistory(user.getUid())));
    }

    @DeleteMapping("/search-history")
    @ApiOperation(value = "검색 히스토리 삭제 api")
    public ResponseEntity<SingleItemResponse> deleteSearchHistory(
            @ApiIgnore @RequestAttribute(AppConstants.REQUEST_USER) User user,
            @RequestParam("type") SearchHistory.Type type) {
        log.info("deleteSearchHistory - type : " + type);
        searchService.deleteSearchHistory(user.getUid(), type);
        return ResponseEntity.ok(SingleItemResponse.create(new EmptyJson()));
    }
}
