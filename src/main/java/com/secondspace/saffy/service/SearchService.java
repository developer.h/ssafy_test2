package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.CurationKeywordDao;
import com.secondspace.saffy.dao.SearchHistoryDao;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.vo.model.SearchHistory;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.search.SearchResultVO;
import com.secondspace.saffy.vo.user.ProfileVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SearchService {
    private final UserDao userDao;
    private final CurationKeywordDao curationKeywordDao;
    private final SearchHistoryDao searchHistoryDao;

    public SearchService(UserDao userDao, CurationKeywordDao curationKeywordDao, SearchHistoryDao searchHistoryDao) {
        this.userDao = userDao;
        this.curationKeywordDao = curationKeywordDao;
        this.searchHistoryDao = searchHistoryDao;
    }

    public SearchResultVO searchByKeyword(String uid, String keyword) {
        List<User> userResult = userDao.searchUsersByKeyword(keyword);

        List<String> keywordResult = curationKeywordDao.searchKeywords(keyword);

        SearchResultVO result = SearchResultVO.builder().query(keyword)
                .users(userResult.stream().map(u -> ProfileVO.from(u)).collect(Collectors.toList()))
                .keywords(keywordResult)
                .build();

        searchHistoryDao.saveAll(result.getUsers().stream()
                .map(p -> SearchHistory.builder()
                        .uid(uid)
                        .resultUid(p.getUid())
                        .build()).collect(Collectors.toList()));

        searchHistoryDao.saveAll(result.getKeywords().stream()
                .map(k -> SearchHistory.builder()
                        .uid(uid)
                        .keyword(k)
                        .build()).collect(Collectors.toList()));

        return result;
    }

    public SearchResultVO getSearchHistory(String uid) {
        List<SearchHistory> searchHistories = searchHistoryDao.findAllByUidOrderByIdDesc(uid);

        Set<String> resultUids = new HashSet<>();
        Set<String> keywords = new HashSet<>();

        for (SearchHistory history : searchHistories) {
            if (history.getResultUid() != null) {
                resultUids.add(history.getResultUid());
            } else if (history.getKeyword() != null) {
                keywords.add(history.getKeyword());
            }
        }

        return SearchResultVO.builder()
                .keywords(keywords)
                .users(userDao.findAllByUidIsIn(resultUids).stream().map(user -> ProfileVO.from(user)).collect(Collectors.toList()))
                .build();
    }

    @Transactional
    public void deleteSearchHistory(String uid, SearchHistory.Type type) {
        if (type == SearchHistory.Type.USER)
            searchHistoryDao.deleteAllByUidAndResultUidIsNotNullAndKeywordIsNull(uid);
        else if (type == SearchHistory.Type.KEYWORD)
            searchHistoryDao.deleteAllByUidAndResultUidIsNullAndKeywordIsNotNull(uid);

    }
}
