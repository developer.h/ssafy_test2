package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.LikeDao;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.model.Likes;

import com.secondspace.saffy.vo.response.RCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class LikeService {
    private final FeedService feedService;
    private final CommentService commentService;
    private final LikeDao likeDao;

    public LikeService(FeedService feedService, CommentService commentService, LikeDao likeDao) {
        this.feedService = feedService;
        this.commentService = commentService;
        this.likeDao = likeDao;
    }

    @Transactional
    public void setLike(String fromUid, String toUid, Likes.TargetType targetType, int targetId, boolean iLiked) {
        if (targetType == Likes.TargetType.COMMENT)
            commentService.checkDBComment(toUid, targetId);
        else if (targetType == Likes.TargetType.FEED)
            feedService.checkDBFeed(toUid, targetId);

        if (iLiked) {
            if(!likeDao.findByUidAndTargetTypeAndTargetId(fromUid, targetType, targetId).isPresent()) {
                Likes likes = Likes.builder().uid(fromUid).targetUid(toUid).targetType(targetType).targetId(targetId).build();
                likeDao.save(likes);
            }
        } else {
            likeDao.deleteByUidAndTargetTypeAndTargetId(fromUid, targetType, targetId);
        }
    }
}
