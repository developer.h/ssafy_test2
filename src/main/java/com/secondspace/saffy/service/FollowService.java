package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.*;
import com.secondspace.saffy.util.CommonUtil;
import com.secondspace.saffy.vo.follow.FollowVO;
import com.secondspace.saffy.vo.follow.FollowerApproveRequest;
import com.secondspace.saffy.vo.model.*;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.user.ProfileVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class FollowService {
    @Autowired
    FollowDao followDao;
    @Autowired
    UserDao userDao;
    @Autowired
    UserPrivacyDao userPrivacyDao;
    @Autowired
    NotiSettingDao notiSettingDao;
    @Autowired
    NotificationDao notificationDao;

    HashMap<Follow.Status, String> followApprovalTypeMap = new HashMap<>();

    @PostConstruct
    public void init() {
        followApprovalTypeMap.put(Follow.Status.APPROVED, "Approved");
        followApprovalTypeMap.put(Follow.Status.DENIED, "Denied");
        followApprovalTypeMap.put(Follow.Status.REQUESTED, "Requested");
    }


    private FollowVO makeFollowingResponse(String callerUid, Follow follow, boolean isMyFollowing) {
        Optional<User> userOpt = userDao.findById(follow.getTargetUid());
        if(!userOpt.isPresent())
            return null;

        Boolean isFollow = null;
        Boolean isFollower = false;
        Follow isFollowingData;
        if(!isMyFollowing) {
            isFollowingData = followDao.getByUidAndTargetUidAndStatus(callerUid, follow.getUid(), Follow.Status.APPROVED);
            if (isFollowingData != null)
                isFollow = true;
            else
                isFollow = false;
        }
        Follow isFollowerData = followDao.getByUidAndTargetUidAndStatus(follow.getUid(), callerUid, Follow.Status.APPROVED);
        if(isFollowerData != null)
            isFollower = true;

        return FollowVO.from(follow, isFollow, isFollower, ProfileVO.from(userOpt.get()));

    }

    private FollowVO makeFollowerResponse(String targetUid, Follow follow) {
        Optional<User> userOpt = userDao.findById(follow.getUid());
        if(!userOpt.isPresent())
            return null;

        Boolean isFollowing = false;
        if(followDao.getByUidAndTargetUid(targetUid, follow.getUid())!= null) {
            isFollowing = true;
        }


        return FollowVO.from(follow, isFollowing, ProfileVO.from(userOpt.get()));

    }

    private FollowVO makeFollowerResponse(String callerId, Follow follow, boolean isMyFollower) {
        Optional<User> userOpt = userDao.findById(follow.getUid());
        if(!userOpt.isPresent())
            return null;

        Boolean isFollowing = false;
        if(followDao.getByUidAndTargetUidAndStatus(callerId, follow.getUid(), Follow.Status.APPROVED) != null) {
            isFollowing = true;
        }
        Boolean isFollowers = null;
        if(!isMyFollower) {
            if (followDao.getByUidAndTargetUidAndStatus(follow.getUid(), callerId, Follow.Status.APPROVED) != null) {
                isFollowers = true;
            } else {
                isFollowers = false;
            }
        }


        return FollowVO.from(follow, isFollowing, isFollowers, ProfileVO.from(userOpt.get()));

    }

    private FollowVO makeWaitApprovalResponse(Follow follow)  {
        Optional<User> userOpt = userDao.findById(follow.getUid());
        if(!userOpt.isPresent())
            return null;

        return FollowVO.from(follow, ProfileVO.from(userOpt.get()));
    }

    public List<FollowVO> getMyFollowers(String uid, PageableRequestParam pageableRequestParam) throws SaffyException {
        try {
            String defaultNextToken = String.valueOf(System.currentTimeMillis()) + "999";
            List<Follow> followerList = followDao.getAllByTargetUidAndStatusForPaging(uid,
                    Follow.Status.APPROVED.getValue(), pageableRequestParam.getPageStartToken(defaultNextToken),
                    pageableRequestParam.getLimit());

            return followerList
                    .stream()
                    .map(follow -> this.makeFollowerResponse(uid, follow, true))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public List<FollowVO> getUserFollowers(String callerId, String targetId, PageableRequestParam pageableRequestParam) throws SaffyException {
        try {
            String defaultNextToken = String.valueOf(System.currentTimeMillis()) + "999";
            List<Follow> followerList = followDao.getAllByTargetUidAndStatusForPaging(targetId,
                    Follow.Status.APPROVED.getValue(), pageableRequestParam.getPageStartToken(defaultNextToken),
                    pageableRequestParam.getLimit());

            return followerList
                    .stream()
                    .map(follow -> this.makeFollowerResponse(callerId, follow, false))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public List<FollowVO> getMyFollowers(String uid) throws SaffyException {
        try {
            List<Follow> followerList = followDao.getAllByTargetUidAndStatus(uid, Follow.Status.APPROVED);

            return followerList
                    .stream()
                    .map(follow -> this.makeFollowerResponse(uid, follow))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public List<FollowVO> getWaitForApprovals(String uid, PageableRequestParam pageableRequestParam) throws SaffyException {
        try {
            String defaultNextToken = String.valueOf(System.currentTimeMillis()) + "999";
            List<Follow> waitForApprovals = followDao.getAllByTargetUidAndStatusForPaging(uid,
                    Follow.Status.REQUESTED.getValue(), pageableRequestParam.getPageStartToken(defaultNextToken),
                    pageableRequestParam.getLimit());

            return waitForApprovals
                    .stream()
                    .map(follow -> this.makeWaitApprovalResponse(follow))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public List<FollowVO> getMyFollowings(String uid, PageableRequestParam pageableRequestParam) {
        try {
            String defaultNextToken = String.valueOf(System.currentTimeMillis()) + "999";
            List<Follow> followerList = followDao.getAllByUidAndStatusForPaging(uid,
                    Follow.Status.APPROVED.getValue(), pageableRequestParam.getPageStartToken(defaultNextToken),
                    pageableRequestParam.getLimit());

            return followerList
                    .stream()
                    .map(follow -> this.makeFollowingResponse(uid, follow, true))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public List<FollowVO> getUserFollowings(String callerId, String targetId, PageableRequestParam pageableRequestParam) {
        try {
            String defaultNextToken = String.valueOf(System.currentTimeMillis()) + "999";
            List<Follow> followerList = followDao.getAllByUidAndStatusForPaging(targetId,
                    Follow.Status.APPROVED.getValue(), pageableRequestParam.getPageStartToken(defaultNextToken),
                    pageableRequestParam.getLimit());

            return followerList
                    .stream()
                    .map(follow -> this.makeFollowingResponse(callerId, follow, false))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public void follow(String callerUid, String targetUid) throws SaffyException {
        try {
            Follow follow = new Follow();
            follow.setUid(callerUid);
            follow.setTargetUid(targetUid);
            Optional<UserPrivacy> userPrivacy = userPrivacyDao.findById(targetUid);
            Follow.Status status;
            if(!userPrivacy.isPresent())
                status = Follow.Status.APPROVED;
            else {
                if(userPrivacy.get().isPrivateMode())
                    status = Follow.Status.REQUESTED;
                else {
                    if(userPrivacy.get().isFollowApproval())
                        status = Follow.Status.APPROVED;
                    else
                        status = Follow.Status.REQUESTED;
                }
            }
            Random random = new Random();
            String approval_date = String.valueOf(System.currentTimeMillis())+String.valueOf(random.nextInt(100));
            follow.setApprovalDate(approval_date);

            follow.setStatus(status);
            followDao.save(follow);

            try {
                NotiSetting notiSettingTarget = notiSettingDao.findByUid(targetUid);
                if (notiSettingTarget != null) {
                    if(status == Follow.Status.REQUESTED) {
                        if(notiSettingTarget.isNewFollowerAccept()) {
                            Notification notification = Notification.builder()
                                    .uid(targetUid).senderUid(callerUid).notiType(Notification.Type.newFollowerAccept)
                                    .build();
                            notificationDao.save(notification);
                        }
                    } else {
                        if(notiSettingTarget.isNewFollower()) {
                            Notification notification = Notification.builder()
                                    .uid(targetUid).senderUid(callerUid).notiType(Notification.Type.newFollower)
                                    .build();
                            notificationDao.save(notification);
                        }
                    }

                }

                if (status == Follow.Status.APPROVED) {
                    NotiSetting notiSettingCaller = notiSettingDao.findByUid(callerUid);
                    if (notiSettingCaller == null && notiSettingCaller.isNewFollowerAcceptResult()) {
                        HashMap<String, String> data =new HashMap<>();
                        data.put("result", followApprovalTypeMap.get(status));
                        String dataStr = CommonUtil.convertObjectToString(data);

                        Notification notification = Notification.builder().uid(callerUid).senderUid(targetUid)
                                .notiType(Notification.Type.newFollowerAcceptResult)
                                .data(dataStr).build();
                        notificationDao.save(notification);
                    }
                }
            } catch (Exception e) {

            }
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    @Transactional
    public void cancelFollow(String callerUid, String targetUid) {
        try {
            Follow follow = followDao.getByUidAndTargetUid(callerUid, targetUid);
            if(follow == null)
                return;

            NotiSetting notiSetting = notiSettingDao.findByUid(targetUid);
            if(notiSetting != null && notiSetting.isCancelFollower()) {
                try {
                    Notification notification = Notification.builder().uid(targetUid)
                            .senderUid(callerUid).notiType(Notification.Type.cancelFollower).build();
                    notificationDao.save(notification);
                } catch (Exception e) {

                }
            }

            followDao.delete(follow);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public void checkAlreadyFollowing(String callerUid, String targetUid) throws SaffyException {
        try {
            Follow follow = followDao.getByUidAndTargetUid(callerUid, targetUid);
            if(follow != null) {
                throw new SaffyException(RCode.ALREADY_FOLLOWING, targetUid);
            }
        } catch(SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public int numberOfFollowers(String uid) throws SaffyException {
        try {
            return followDao.countByTargetUidAndStatus(uid, Follow.Status.APPROVED);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null,  e);
        }
    }

    public int numberOfFollowings(String uid) throws SaffyException {
        try {
            return followDao.countByUidAndStatus(uid, Follow.Status.APPROVED);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null,  e);
        }
    }


    public void approve(String userId, FollowerApproveRequest approvalRequest) throws SaffyException{
        User user = userDao.findById(approvalRequest.getUid()).orElse(null);
        try {
            if(user == null)
                throw new SaffyException(RCode.CANNOT_FIND_USER_FOR_FOLLOWER_APPROVAL);
            Follow follow = followDao.getByUidAndTargetUid(approvalRequest.getUid(), userId);
            if(follow == null) {
                throw new SaffyException(RCode.CANNOT_FIND_APPROVAL_REQUEST);
            }

            follow.setStatus(approvalRequest.getType());
            if(Follow.Status.APPROVED == approvalRequest.getType()) {
                Random random =new Random();
                String approval_date = String.valueOf(System.currentTimeMillis())+String.valueOf(random.nextInt(100));
                follow.setApprovalDate(approval_date);
            }
            followDao.save(follow);

            try {
                NotiSetting notiSetting = notiSettingDao.findByUid(approvalRequest.getUid());
                if(notiSetting == null) return;

                if(!notiSetting.isNewFollowerAcceptResult()) return;
                Map<String, String> data = new HashMap<>();

                data.put("result", followApprovalTypeMap.get(approvalRequest.getType()));
                String dataStr = CommonUtil.convertObjectToString(data);

                Notification notification = Notification.builder()
                        .uid(approvalRequest.getUid())
                        .senderUid(userId)
                        .notiType(Notification.Type.newFollowerAcceptResult)
                        .data(dataStr)
                        .build();

                notificationDao.save(notification);

            } catch (Exception e) {

            }
        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }

    }

    public boolean isFollowing(String uid, String targetUid) throws SaffyException {
        try {
            Follow follow = followDao.getByUidAndTargetUidAndStatus(uid, targetUid, Follow.Status.APPROVED);
            if(follow != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public void checkPermission(String uid, String targetUid){
        if(uid.equals(targetUid))
            return;

        UserPrivacy pvc = userPrivacyDao.findByUid(targetUid);
        if(pvc == null || !pvc.isPrivateMode()){
            return;
        }else if(isFollowing(uid, targetUid)){
            return;
        }else
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);
    }
}
