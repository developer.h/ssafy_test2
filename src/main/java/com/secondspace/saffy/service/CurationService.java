package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.*;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.util.CommonUtil;
import com.secondspace.saffy.vo.curation.CurationSetVO;
import com.secondspace.saffy.vo.feed.ArticleCurationVO;
import com.secondspace.saffy.vo.model.*;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CurationService {
    private final CurationSetDao curationSetDao;

    private final CurationSourceDao curationSourceDao;

    private final CurationKeywordDao curationKeywordDao;

    private final SearchSourceDao searchSourceDao;

    private final CurationCrawler curationCrawler;

    private final KeywordLogDao keywordLogDao;

    private final ArticleDao articleDao;


    public CurationService(CurationSetDao curationSetDao, CurationSourceDao curationSourceDao, CurationKeywordDao curationKeywordDao, SearchSourceDao searchSourceDao, CurationCrawler curationCrawler, KeywordLogDao keywordLogDao, ArticleDao articleDao) {
        this.curationSetDao = curationSetDao;
        this.curationSourceDao = curationSourceDao;
        this.curationKeywordDao = curationKeywordDao;
        this.searchSourceDao = searchSourceDao;
        this.curationCrawler = curationCrawler;
        this.keywordLogDao = keywordLogDao;
        this.articleDao = articleDao;
    }

    public List<ArticleCurationVO> crawlCuration(String uid, Integer[] curationIds) {
        List<CurationSet> curationList = curationSetDao.findAllById(Arrays.asList(curationIds));

        if (curationList.size() == 0)
            new SaffyException(RCode.RESOURCE_NOT_EXISTS, "curation-set is not exists");


        if (curationList.stream().anyMatch(curation -> !curation.getUid().equals(uid)))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);

        List<ArticleCurationVO> result = new ArrayList();

        for (CurationSet curation : curationList) {
            List<String> keywords = curationKeywordDao.findAllByCurationId(curation.getId()).stream()
                    .map(curationKeyword -> curationKeyword.getKeyword())
                    .collect(Collectors.toList());

            List<SearchSource> searchSources = curationSourceDao.findAllByCurationId(curation.getId()).stream()
                    .map(curationSource -> searchSourceDao.findById(curationSource.getSearchSourceId()).orElse(null))
                    .filter(item -> item != null).collect(Collectors.toList());

            Collection<ArticleCurationVO> articles = curationCrawler.startCurationCrawling(keywords, searchSources).stream()
                    .map(a -> ArticleCurationVO.from(a, keywords)).collect(Collectors.toList());
            result.addAll(articles);

        }

        return result.stream().filter(a->!articleDao.existsByUidAndUrl(uid, a.getUrl())).collect(Collectors.toList());
    }


    @Transactional
    public void createCurationSet(String uid, String name, List<String> keywords, List<Integer> searchSources) {
        CurationSet curationSet = curationSetDao.save(CurationSet.builder().uid(uid).name(name).build());

        List<CurationKeyword> keywordList = new ArrayList<>();
        for (String keyword : keywords) {
            keywordList.add(CurationKeyword.builder().curationId(curationSet.getId())
                    .keyword(keyword).build());
        }
        saveCurationKeyword(keywordList);

        for (Integer searchSourceId : searchSources) {
            Optional<SearchSource> searchSource = searchSourceDao.findById(searchSourceId);
            if (searchSource.isPresent()) {
                curationSourceDao.save(CurationSource.builder().curationId(curationSet.getId()).searchSourceId(searchSourceId).build());
            }
        }
    }

    private CurationSetVO getCurationSetWithDetails(CurationSet curation) {
        return CurationSetVO.builder()
                .id(curation.getId())
                .name(curation.getName())
                .keywords(
                        curationKeywordDao.findAllByCurationId(curation.getId()).stream().map(
                                curationKeyword -> CurationSetVO.CurationKeywordVO.builder()
                                        //.id(curationKeyword.getId())
                                        .keyword(curationKeyword.getKeyword())
                                        .build()
                        ).collect(Collectors.toList())
                )
                .searchSources(
                        curationSourceDao.findAllByCurationId(curation.getId()).stream().map(
                                curationSource -> CurationSetVO.CurationSourceVO.builder()
                                        //.id(curationSource.getId())
                                        .searchSourceId(curationSource.getSearchSourceId())
                                        .build()
                        ).map(
                                curationSourceVO -> {
                                    searchSourceDao.findById(curationSourceVO.getSearchSourceId()).ifPresent(
                                            searchSource -> {
                                                curationSourceVO.setTitle(searchSource.getTitle());
                                                curationSourceVO.setType(searchSource.getType());

                                                curationSourceVO.setRssUrl(searchSource.getRssUrl());
                                                curationSourceVO.setGceId(searchSource.getGceId());
                                                curationSourceVO.setGceApiKey(searchSource.getGceApiKey());
                                            }
                                    );
                                    return curationSourceVO;
                                }
                        ).collect(Collectors.toList())
                )
                .build();
    }

    public List<CurationSetVO> getAllCurationSetWithDetails(String uid, PageableRequestParam pageParam) {
        return curationSetDao.findAllByUid(uid,
                pageParam.getPageStartToken(0), pageParam.getLimit()).stream().map(curation -> getCurationSetWithDetails(curation))
                .collect(Collectors.toList());
    }

    public CurationSetVO getCurationSetWithDetails(String uid, int curationId) {
        CurationSet curation = curationSetDao.findById(curationId)
                .orElseThrow(() -> new SaffyException(RCode.RESOURCE_NOT_EXISTS, "curation-set is not exists"));

        return getCurationSetWithDetails(curation);
    }

    @Transactional
    public void editCurationSet(String uid, String scrapSourceUid, int id, String name,
                                List<String> addedKeywords, List<String> deletedKeywords,
                                List<Integer> addedSearchSources, List<Integer> deletedSearchSources) {
        CurationSet curationSet = curationSetDao.findById(id).orElse(null);
        if (curationSet == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "curation-set is not exists");
        if (!curationSet.getUid().equals(uid))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);

        if (CommonUtil.isValid(name)) {
            curationSet.setName(name);
            curationSetDao.save(curationSet);
        }

        if (CommonUtil.isValid(addedKeywords)) {
            saveCurationKeyword(
                    addedKeywords.stream().map(
                            keyword -> CurationKeyword.builder()
                                    .curationId(curationSet.getId())
                                    .keyword(keyword)
                                    .build()
                    ).collect(Collectors.toList())
            );
        }

        if (CommonUtil.isValid(deletedKeywords)) {
            for (String keyword : deletedKeywords) {
                curationKeywordDao.deleteByCurationIdAndKeyword(curationSet.getId(), keyword);
            }
        }

        if (CommonUtil.isValid(addedSearchSources)) {
            if (scrapSourceUid == null) {
                curationSourceDao.saveAll(
                        addedSearchSources.stream().map(
                                searchSourceId -> CurationSource.builder()
                                        .curationId(curationSet.getId())
                                        .searchSourceId(searchSourceId)
                                        .build()
                        ).collect(Collectors.toList())
                );
            } else {
                for (int searchSourceId : addedSearchSources) {
                    SearchSource from = searchSourceDao.findById(searchSourceId).orElse(null);
                    if (from != null && from.getUid().equals(scrapSourceUid)) {
                        SearchSource searchSource = searchSourceDao.save(
                                SearchSource.builder().uid(uid).type(from.getType()).title(from.getTitle())
                                        .rssUrl(from.getRssUrl()).build());

                        curationSourceDao.save(CurationSource.builder().curationId(curationSet.getId()).searchSourceId(searchSource.getId()).build());
                    }
                }
            }
        }

        if (CommonUtil.isValid(deletedSearchSources)) {
            for (int searchSourceId : deletedSearchSources) {
                curationSourceDao.deleteByCurationIdAndSearchSourceId(curationSet.getId(), searchSourceId);
            }
        }

    }

    @Transactional
    public void deleteCurationSet(String uid, int curationSetId) {
        CurationSet curationSet = curationSetDao.findById(curationSetId).orElse(null);
        if (curationSet == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "curation-set is not exists");
        if (!curationSet.getUid().equals(uid))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);

        curationSourceDao.deleteAllByCurationId(curationSetId);
        curationKeywordDao.deleteAllByCurationId(curationSetId);

        curationSetDao.deleteById(curationSetId);
    }

    private void saveCurationKeyword(List<CurationKeyword> keywords) {
        curationKeywordDao.saveAll(keywords);
        keywordLogDao.saveAll(keywords.stream().map(k -> KeywordLog.builder()
                .keyword(k.getKeyword())
                .type(KeywordLog.Type.REGISTRATION)
                .build()).collect(Collectors.toList()));

    }

}
