package com.secondspace.saffy.service;

import com.secondspace.saffy.auth.JwtToken;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.dao.UserSnsDao;
import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.model.UserSns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationService {
    @Autowired
    UserDao userDao;
    @Autowired
    UserSnsDao userSnsDao;
    @Autowired
    UserService userService;
    @Autowired
    JwtToken jwtToken;

    public String emailPasswordAuthenticate(String email, String password) throws SaffyException {
        User user;
        try {
            user = userDao.getUserByEmail(email);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }

        if (user == null) {
            throw new SaffyException(RCode.CANNOT_FIND_USER_FOR_AUTH, email);
        }

        if (user.getPassword() == null || !user.getPassword().equals(password)) {
            throw new SaffyException(RCode.PASSWORD_NOT_MATCHED);
        }

        String token =  jwtToken.generateToken(user.getUid());

        if(token == null) {
            throw new SaffyException(RCode.TOKEN_GENERATION_ERROR);
        }

        return token;
    }

    public String socialAuthenticate(String snsUid, String email, String snsType) throws SaffyException{
        UserSns userSns;
        try {
            userSns = userSnsDao.getBySnsUid(snsUid);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }

        if(userSns == null) {
            userSns = userService.signUpWithSns(snsUid, email, snsType);
        }

        String token =  jwtToken.generateToken(userSns.getUid());

        if(token == null) {
            throw new SaffyException(RCode.TOKEN_GENERATION_ERROR);
        }

        return token;
    }
}
