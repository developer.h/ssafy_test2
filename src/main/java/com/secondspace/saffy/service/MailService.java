package com.secondspace.saffy.service;

import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.util.MailUtil;
import com.secondspace.saffy.util.TempKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender;

    @Value("${saffy.mail.sender.name}")
    private String senderName;

    @Value("${saffy.mail.sender.email}")
    private String senderEmail;

    private String sendMail(String uid, String email, String code, String subject, String htmlContent){
        try {
            // 임의의 authkey 생성
            String authKey = new TempKey().getKey(50, false);

            // mail 작성 관련
            MailUtil sendMail = new MailUtil(mailSender);

            sendMail.setSubject(subject);
            sendMail.setText(htmlContent);
            sendMail.setFrom(senderEmail, senderName);
            sendMail.setTo(email);
            sendMail.send();

            return authKey;
        }catch(UnsupportedEncodingException | MessagingException mailException){
            throw new SaffyException(RCode.BAD_CREDENTIAL, mailException);
        }
    }

    public String sendMailToValidateAccount(String uid, String email, String code) {
        String subject = "SecondSpace 회원가입 이메일 인증";
        String htmlContent = new StringBuffer().append("<h1>[이메일 인증]</h1>")
                .append("<p>아래 링크를 클릭하시면 이메일 인증이 완료됩니다.</p>")
                .append("https://ssafy-project-60f77.firebaseapp.com//#/user/auth/"+code)
                .append("' target='_blenk'>이메일 인증 확인</a>")
                .toString();
        return sendMail(uid, email, code, subject, htmlContent);
    }

    public String sendMailToChangePassword(String uid, String email, String code) {
        String subject = "SecondeSpace 비밀번호 변경 인증";
        String htmlContent = new StringBuffer().append("<h1>[비밀번호 변경]</h1>")
                .append("<p>아래 링크를 클릭하시면 비밀번호를 변경하실 수 있습니다.</p>")
                .append("https://ssafy-project-60f77.firebaseapp.com//#/user/changePassword/"+code)
                .append("' target='_blenk'>이메일 인증 확인</a>")
                .toString();
        return sendMail(uid, email, code, subject, htmlContent);
    }
}
