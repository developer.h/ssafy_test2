package com.secondspace.saffy.service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.secondspace.saffy.vo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.dao.FollowDao;
import com.secondspace.saffy.dao.NotiSettingDao;
import com.secondspace.saffy.dao.NotificationDao;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.dao.UserEmailValidationDao;
import com.secondspace.saffy.dao.UserPrivacyDao;
import com.secondspace.saffy.dao.UserSnsDao;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.util.UidUtil;
import com.secondspace.saffy.vo.auth.PasswordChangeRequest;
import com.secondspace.saffy.vo.auth.PasswordChangeRequestRequest;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.vo.user.NotificationSettingVO;
import com.secondspace.saffy.vo.user.NotificationVO;
import com.secondspace.saffy.vo.user.ProfileVO;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserDao userDao;
    @Autowired
    UserSnsDao userSnsDao;
    @Autowired
    UserPrivacyDao userPrivacyDao;
    @Autowired
    NotiSettingDao notiSettingDao;
    @Autowired
    FollowDao followDao;
    @Autowired
    NotificationDao notificationDao;
    @Autowired
    MailService mailService;
    @Autowired
    UserEmailValidationDao userEmailValidationDao;

    private void checkInUseEmail(String email) throws SaffyException {
        User user = userDao.getUserByEmail(email);
        if (user != null) {
            throw new SaffyException(RCode.ALREADY_JOINED, email, null);
        }
    }

    private void checkInUseNickname(String nickname) throws SaffyException {
        User user = userDao.getUserByNickname(nickname);
        if (user != null) {
            throw new SaffyException(RCode.NICKNAME_ALREADY_IN_USE, nickname, null);
        }
    }

    private UserSns.SnsType getSnsType(String snsType) {
        if (snsType.equals(ApiConstant.socialTypeGoogle)) {
            return UserSns.SnsType.GOOGLE;
        } else if (snsType.equals(ApiConstant.socialTypeKAKAO)) {
            return UserSns.SnsType.KAKAO;
        } else {
            return UserSns.SnsType.NAVER;
        }
    }

    private User insertUser(String email, String password, String nickname) throws SaffyException {
        String uid = UidUtil.generateUid();
        User user = new User();
        user.setUid(uid);
        user.setEmail(email);
        user.setPassword(password);
        user.setNickname(nickname);
        user.setEmailCheck(false);

        try {
            userDao.save(user);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }

        return user;
    }

    private void insertUserPrivacy(String uid, boolean isPrivateMode, Boolean isAutoFollowApproval) {
        try {
            UserPrivacy userPrivacy = UserPrivacy.builder().uid(uid)
                    .privateMode(isPrivateMode)
                    .followApproval(isAutoFollowApproval)
                    .build();
            userPrivacyDao.save(userPrivacy);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    private UserSns insertUserSns(String uid, String snsUid, String snsType) throws SaffyException {
        UserSns userSns = new UserSns();
        userSns.setUid(uid);
        userSns.setSnsUid(snsUid);
        userSns.setSnsType(getSnsType(snsType));
        try {
            userSnsDao.save(userSns);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }

        return userSns;
    }

    private void createNotiSettigs(String uid) throws SaffyException {
        NotiSetting notiSetting = new NotiSetting(uid, true, true, true, true, true, true, true, true, null, null);
        try {
            notiSettingDao.save(notiSetting);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }


    public void updateNotiSettigs(String uid, NotificationSettingVO noti) throws SaffyException {
        NotiSetting notiSetting = notiSettingDao.findByUid(uid);

        if (notiSetting == null) {
            notiSetting = new NotiSetting();
            notiSetting.setUid(uid);
        }

        if (noti.getCancelFollower() != null)
            notiSetting.setCancelFollower(noti.getCancelFollower());
        if (noti.getFollowerNewFeed() != null)
            notiSetting.setFollowerNewFeed(noti.getFollowerNewFeed());
        if (noti.getMyFeedComment() != null) notiSetting.setMyFeedComment(noti.getMyFeedComment());
        if (noti.getMyFeedLike() != null) notiSetting.setMyFeedLike(noti.getMyFeedLike());
        if (noti.getMyFeedScrap() != null) notiSetting.setMyFeedScrap(noti.getMyFeedScrap());
        if (noti.getNewFollower() != null) notiSetting.setNewFollower(noti.getNewFollower());
        if (noti.getNewFollowerAccept() != null)
            notiSetting.setNewFollowerAccept(noti.getNewFollowerAccept());
        if (noti.getNewFollowerAcceptResult() != null)
            notiSetting.setNewFollowerAcceptResult(noti.getNewFollowerAcceptResult());

        try {
            notiSettingDao.save(notiSetting);
        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    @Transactional
    public User signUpWithEmail(String email, String password, String nickname) throws SaffyException {
        checkInUseEmail(email);
        checkInUseNickname(nickname);

        User user = insertUser(email, password, nickname);
        insertUserPrivacy(user.getUid(), false, false);
        createNotiSettigs(user.getUid());
        startEMailValidation(user.getUid(), email);
        return user;
    }

    public UserSns signUpWithSns(String snsUid, String email, String snsType) throws SaffyException {
        checkInUseEmail(email);

        User user;
        try {
            user = insertUser(email, null, null);
        } catch (SaffyException e) {
            throw e;
        }

        try {
            UserSns userSns = insertUserSns(user.getUid(), snsUid, snsType);
            return userSns;
        } catch (SaffyException e) {
            userDao.delete(user);
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public User getUser(String uid) throws SaffyException {
        try {
            Optional<User> user = userDao.findById(uid);
            return user.isPresent() ? user.get() : null;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public UserSns getUserSnsByUid(String uid) throws SaffyException {
        try {
            UserSns userSns = userSnsDao.getByUid(uid);
            return userSns;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public User getUserByNickname(String nickname) throws SaffyException {
        try {
            return userDao.getUserByNickname(nickname);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public void updateUser(String userId, String nickname, String description,
                           String profileImage, String contentType) throws SaffyException {
        try {
            Optional<User> userOpt = userDao.findById(userId);

            User user = userOpt.get();
            if (nickname != null)
                user.setNickname(nickname);
            if (description != null)
                user.setDescription(description);
            if (profileImage != null)
                user.setProfileImage(profileImage);
            if (contentType != null)
                user.setContentType(contentType);

            userDao.save(user);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public void updateUserPrivacy(String userId,
                                  Boolean isPublic,
                                  Boolean isFollowAccept) throws SaffyException {
        try {
            Optional<UserPrivacy> userPrivacyOpt = userPrivacyDao.findById(userId);
            UserPrivacy userPrivacy;
            if (!userPrivacyOpt.isPresent()) {
                userPrivacy = new UserPrivacy();
                userPrivacy.setUid(userId);
                userPrivacy.setFollowApproval(true);
                userPrivacy.setPrivateMode(false);
            } else {
                userPrivacy = userPrivacyOpt.get();
            }

            if (isPublic != null)
                userPrivacy.setPrivateMode(!isPublic);
            if (isFollowAccept != null)
                userPrivacy.setFollowApproval(isFollowAccept);

            userPrivacyDao.save(userPrivacy);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, null, e);
        }
    }

    public ProfileVO getMyProfile(String uid) throws SaffyException {
        try {
            Optional<User> userOpt = userDao.findById(uid);
            if (!userOpt.isPresent()) {
                throw new SaffyException(RCode.CANNOT_FIND_USER_FOR_PROFILE, "uid");
            }

            int numberOfFollowings = followDao.countByUidAndStatus(uid, Follow.Status.APPROVED);
            int numberOfFollowers = followDao.countByTargetUidAndStatus(uid, Follow.Status.APPROVED);

            User user = userOpt.get();
            NotiSetting notiSetting = notiSettingDao.findByUid(uid);
            UserPrivacy userPrivacy = userPrivacyDao.findByUid(uid);
            return ProfileVO.from(user, numberOfFollowings, numberOfFollowers, notiSetting, userPrivacy);
        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public ProfileVO getUserProfile(String callerId, String targetId) {
        try {
            Optional<User> targetUserOpt = userDao.findById(targetId);
            if (!targetUserOpt.isPresent()) {
                throw new SaffyException(RCode.CANNOT_FIND_USER_FOR_PROFILE, "uid");
            }
            User targetUser = targetUserOpt.get();
            int numberOfFollowings = followDao.countByUidAndStatus(targetUser.getUid(), Follow.Status.APPROVED);
            int numberOfFollowers = followDao.countByTargetUidAndStatus(targetUser.getUid(), Follow.Status.APPROVED);
            boolean isFollowing = false;
            boolean isFollower = false;
            Follow.Status status = null;
            Follow follow = followDao.getByUidAndTargetUid(callerId, targetId);
            if (follow != null) {
                if(follow.getStatus() == Follow.Status.APPROVED) {
                    isFollowing = true;
                    status = follow.getStatus();
                } else {
                    status = follow.getStatus();
                }
            }
            if (followDao.getByUidAndTargetUidAndStatus(targetId, callerId, Follow.Status.APPROVED) != null) {
                isFollower = true;
            }

            UserPrivacy userPrivacy = userPrivacyDao.findByUid(targetId);

            return ProfileVO.from(targetUser, userPrivacy, numberOfFollowings, numberOfFollowers, isFollower, isFollowing, status);
        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    private void startEMailValidation(String uid, String email) throws SaffyException {
        try {
            String code = UUID.randomUUID().toString();
            UserEmailValidation emailValidation = new UserEmailValidation();
            emailValidation.setCode(code);
            emailValidation.setUid(uid);
            userEmailValidationDao.save(emailValidation);
            mailService.sendMailToValidateAccount(uid, email, code);
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public void validateCode(String code) {
        try {
            UserEmailValidation emailValidation = userEmailValidationDao.findByCode(code);
            if (emailValidation == null) throw new SaffyException(RCode.INVALID_VALIDATION_CODE);


            User user = userDao.findById(emailValidation.getUid()).orElse(null);
            if (user == null) throw new SaffyException(RCode.INVALID_VALIDATION_CODE);

            user.setEmailCheck(true);
            userDao.save(user);
            userEmailValidationDao.deleteById(code);

        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public void changePasswordRequest(PasswordChangeRequestRequest password) throws SaffyException {
        try {
            if (password.getEmail() != null) {
                User user = userDao.getUserByEmail(password.getEmail());
                if (user == null)
                    throw new SaffyException(RCode.CANNOT_FIND_EMAIL, password.getEmail());

                String code = UUID.randomUUID().toString();
                UserEmailValidation emailValidation = new UserEmailValidation();
                emailValidation.setCode(code);
                emailValidation.setUid(user.getUid());
                userEmailValidationDao.save(emailValidation);

                mailService.sendMailToChangePassword(user.getUid(), user.getEmail(), code);
            } else {

            }
        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
    }

    public void changePassword(PasswordChangeRequest password) {
        try {
            UserEmailValidation emailValidation = userEmailValidationDao.findByCode(password.getCode());
            if (emailValidation == null) throw new SaffyException(RCode.INVALID_VALIDATION_CODE);

            User user = userDao.findById(emailValidation.getUid()).orElse(null);
            if (user == null) throw new SaffyException(RCode.INVALID_VALIDATION_CODE);

            if (user.getPassword().equals(password.getPassword()))
                throw new SaffyException(RCode.INVALID_PASSWORD);
            user.setPassword(password.getPassword());

            userDao.save(user);
            userEmailValidationDao.delete(emailValidation);

        } catch (SaffyException e) {
            throw e;
        } catch (Exception e) {

        }
    }
}
