package com.secondspace.saffy.service;

import com.secondspace.saffy.exception.ErrorCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.mapper.UserMapper;
import com.secondspace.saffy.vo.db.UserTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDatabaseService {
    @Autowired
    UserMapper userMapper;
    public UserTable getUserTableByEmail(String email) throws SaffyException {
        try {
            return userMapper.selectUserByEmail(email);
        } catch (Exception e) {
            throw new SaffyException(ErrorCode.DATABASE_ERROR, e);
        }
    }

    public UserTable getUserTableByUid(String uid) throws SaffyException {
        try {
            return userMapper.selectUserByUid(uid);
        } catch (Exception e) {
            throw new SaffyException(ErrorCode.DATABASE_ERROR, e);
        }
    }

    public void addUser(UserTable userTable) throws SaffyException{
        try {
            userMapper.insertUser(userTable.getUid(), userTable.getEmail(), userTable.getPassword(),
                    userTable.getNick_name(), userTable.getSocial_type());
        } catch (Exception e) {
            throw new SaffyException(ErrorCode.DATABASE_ERROR, null, e);
        }
    }
}
