package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.CurationSourceDao;
import com.secondspace.saffy.dao.SearchSourceDao;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.util.CommonUtil;
import com.secondspace.saffy.vo.curation.SearchSourceVO;
import com.secondspace.saffy.vo.model.SearchSource;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchSourceService {
    private final SearchSourceDao searchSourceDao;
    private final CurationSourceDao curationSourceDao;


    public SearchSourceService(SearchSourceDao searchSourceDao, CurationSourceDao curationSourceDao) {
        this.searchSourceDao = searchSourceDao;
        this.curationSourceDao = curationSourceDao;
    }

    public List<SearchSourceVO> getAllSearchSources(String uid, PageableRequestParam pageParam) {
        List<SearchSourceVO> searchSources = searchSourceDao.findAllByUid(uid,
                pageParam.getPageStartToken(Integer.MIN_VALUE), pageParam.getLimit()).stream().map(
                item -> SearchSourceVO.builder().id(item.getId()).type(item.getType()).title(item.getTitle())
                        .rssUrl(item.getRssUrl()).gceId(item.getGceId()).gceApiKey(item.getGceApiKey()).build()
        ).collect(Collectors.toList());
        return searchSources;
    }

    public void createSearchSource(String uid, String title, SearchSource.Type type,
                                   String rssUrl, String gceId, String gceApiKey) {
        SearchSource searchSource = SearchSource.builder().uid(uid).title(title).type(type).rssUrl(rssUrl)
                .gceId(gceId).gceApiKey(gceApiKey).build();
        searchSourceDao.save(searchSource);
    }

    public SearchSourceVO getSearchSource(String uid, int searchSourceId) {
        SearchSource item = checkDBItem(uid, searchSourceId);

        return SearchSourceVO.builder().id(item.getId()).type(item.getType()).title(item.getTitle())
                .rssUrl(item.getRssUrl()).gceId(item.getGceId()).gceApiKey(item.getGceApiKey()).build();
    }

    public void updateSearchSource(String uid, int searchSourceId, String title, SearchSource.Type type, String rssUrl, String gceId, String gceApiKey) {
        searchSourceDao.findById(searchSourceId).ifPresent(item -> {
            if (!uid.equals(item.getUid()))
                throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);

            if (CommonUtil.isValid(title))
                item.setTitle(title);
            if (CommonUtil.isValid(rssUrl))
                item.setRssUrl(rssUrl);
            if (CommonUtil.isValid(gceId))
                item.setGceId(gceId);
            if (CommonUtil.isValid(gceApiKey))
                item.setGceApiKey(gceApiKey);

            searchSourceDao.save(item);
        });
    }

    @Transactional
    public void deleteSearchSource(String uid, int searchSourceId) {
        searchSourceDao.findById(searchSourceId).ifPresent(item -> {
            if (!uid.equals(item.getUid()))
                throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);

            curationSourceDao.deleteAllBySearchSourceId(searchSourceId);

            searchSourceDao.delete(item);
        });

    }

    public SearchSource checkDBItem(String owner, int id) {
        SearchSource item = searchSourceDao.findById(id).orElse(null);
        if (item == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "SearchSource is not exists");
        if (owner != null && !item.getUid().equals(owner))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);
        return item;
    }
}
