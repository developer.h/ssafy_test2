package com.secondspace.saffy.service;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.util.CommonUtil;
import com.secondspace.saffy.vo.model.Article;
import com.secondspace.saffy.vo.model.SearchSource;
import com.secondspace.saffy.vo.response.RCode;

import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@Slf4j
public class CurationCrawler {
    private final RestTemplate restTemplate;

    public CurationCrawler(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private static final List<SearchSource> DEFAULT_GCE_SOURCE = Arrays.asList(SearchSource.builder()
            .gceApiKey("AIzaSyBzl7rJtmxW8-BZwozv1KtBx-7cQ4S3p0g")
            .gceId("008012530695954790396:38ume8i2tfy").type(SearchSource.Type.GCE).build());

    public Collection<Article> startCurationCrawling(List<String> keywords, List<SearchSource> searchSources) {
        if (!CommonUtil.isValid(searchSources))
            searchSources = DEFAULT_GCE_SOURCE;

        Map<String, Article> result = new HashMap<>();
        for (SearchSource searchSource : searchSources) {
            try {
                if (searchSource.getType() == SearchSource.Type.RSS) {
                    Map<String, Article> articles = crawlFromRSS(searchSource.getRssUrl(), keywords);
                    result.putAll(articles);
                } else if (searchSource.getType() == SearchSource.Type.GCE) {
                    Map<String, Article> articles = crawlFromGCE(searchSource.getGceId(), searchSource.getGceApiKey(), keywords);
                    result.putAll(articles);
                }
            }catch(Exception e){
                log.error("curation crawl error.. ignore this result", e);
            }
        }
        return result.values();
    }

    private Map<String, Article> crawlFromRSS(String url, List<String> keywords) {
        Map<String, Article> result = new HashMap<>();
        try (XmlReader reader = new XmlReader(new URL(url))) {
            SyndFeed feed = new SyndFeedInput().build(reader);
            for (SyndEntry entry : feed.getEntries()) {
                String description = entry.getDescription().getValue()
                        .replaceAll("(\r\n|\r|\n|\n\r)", " ") // 줄 개행 제거
                        .replaceAll("<script[^<]+</script>", "") // <script>~~</script> 제거
                        .replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "") //html 태그 제고
                        .replaceAll("<!--[^<!--]+-->", "")  // html 주석 제거
                        .replaceAll("^\\s+", ""); // 시작 공백 제거

                if (keywords.stream().allMatch(keyword -> description.contains(keyword))) {
                    result.put(entry.getLink()
                            , Article.builder()
                                    .title(entry.getTitle())
                                    .url(entry.getLink())
                                    .description(description)
                                    .build());
                }
            }
        } catch (Exception e) {
            throw new SaffyException(RCode.DATABASE_ERROR, e);
        }
        return result;
    }

    private Map<String, Article> crawlFromGCE(String gceId, String apiKey, List<String> keywords) {
        Map<String, Article> result = new HashMap<>();

        String urlTemplate = "https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&q=%s&start=%s";
        for (int i = 1; i < 100; i += 10) {
            String url = String.format(urlTemplate,
                    apiKey, gceId,
                    String.join("+", keywords), i);

            String responseStr = restTemplate.getForObject(URI.create(url), String.class);
            GCEResult response = restTemplate.getForObject(URI.create(url), GCEResult.class);
            if (response != null && response.items != null) {
                for (GCEResultItem item : response.items) {
                    result.put(item.getLink()
                            , Article.builder()
                                    .title(item.getTitle())
                                    .url(item.getLink())
                                    .description(item.getSnippet())
                                    .mainImage(item.getThumbnailImage())
                                    .build());
                }
            }
        }
        return result;
    }

    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class GCEResult {
        private String kind;
        private List<GCEResultItem> items;
    }

    @Data
    @ToString
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class GCEResultItem {
        private String title;
        private String link;
        private String snippet;
        private String cacheId;

        private GCEResultItemPageMap pagemap;

        public String getThumbnailImage() {
            return Optional.ofNullable(pagemap).map(GCEResultItemPageMap::getCse_thumbnail)
                    .map(List::stream).orElse(Stream.empty())
                    .findFirst().map(GCEResultItemThumbnail::getSrc).orElse(null);
        }
    }

    @Data
    @ToString
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class GCEResultItemPageMap {
        private List<GCEResultItemThumbnail> cse_thumbnail;
    }

    @Data
    @ToString
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class GCEResultItemThumbnail {
        private String src;
    }
}
