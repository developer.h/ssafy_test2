package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.*;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.feed.CommentVO;
import com.secondspace.saffy.vo.model.Comment;
import com.secondspace.saffy.vo.model.Feed;
import com.secondspace.saffy.vo.model.Likes;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CommentService {
    private final FeedService feedService;
    private final FeedDao feedDao;
    private final ArticleDao articleDao;
    private final LikeDao likeDao;
    private final UserDao userDao;
    private final CommentDao commentDao;

    public CommentService(FeedService feedService, ArticleDao articleDao, FeedDao feedDao, LikeDao likeDao, UserDao userDao, CommentDao commentDao) {
        this.feedService = feedService;
        this.feedDao = feedDao;
        this.articleDao = articleDao;
        this.likeDao = likeDao;
        this.userDao = userDao;
        this.commentDao = commentDao;
    }

    public CommentVO createComment(User user, int feedId, Integer parentId, String message) {
        Feed feed = feedService.checkDBFeed(null, feedId);
        Comment parentComment = parentId != null ? checkDBComment(null, parentId) : null;
        Comment comment = Comment.builder()
                .uid(user.getUid()).targetUid(parentId != null ? parentComment.getUid() : feed.getUid())
                .feedId(feedId).parentId(parentId).message(message).build();
        commentDao.save(comment);
        return CommentVO.from(user, comment, null, null);
    }

    public List<CommentVO> getAllCommentsByFeed(User user, int feedId, PageableRequestParam pageParam) {
        List<Comment> comments = commentDao.findAllByFeedIdOrderByIdDesc(feedId,
                pageParam.getPageStartToken(Integer.MAX_VALUE), pageParam.getLimit());
        Map<String, User> userMap = userDao.findAllByUidIsIn(comments.stream().map(c -> c.getUid()).collect(Collectors.toList()))
                .stream().collect(Collectors.toMap(User::getUid, Function.identity()));

        Map<Integer, List<CommentVO>> subCommentMap = new HashMap<>();
        Iterator<Comment> iter = comments.iterator();
        while (iter.hasNext()) {
            Comment subComment = iter.next();
            if (subComment.getParentId() != null) {
                if (!subCommentMap.containsKey(subComment.getParentId()))
                    subCommentMap.put(subComment.getParentId(), new ArrayList<>());
                subCommentMap.get(subComment.getParentId()).add(makeCommentVO(userMap.get(subComment.getUid()), subComment, null));
                iter.remove();
            }
        }

        return comments.stream().filter(comment -> comment.getParentId() == null)
                .map(comment -> makeCommentVO(userMap.get(comment.getUid()), comment, subCommentMap.get(comment.getId()))).collect(Collectors.toList());
    }

    public CommentVO getCommentById(User user, int commentId) {
        Comment comment = commentDao.findById(commentId).orElse(null);
        if (comment == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "comment is not exists");

        return getCommentVO(comment);
    }

    private CommentVO getCommentVO(Comment comment) {
        User owner = userDao.getOne(comment.getUid());

        List<Comment> subComments = commentDao.findAllByParentIdOrderByIdDesc(comment.getId());

        Map<String, User> userMap = userDao.findAllByUidIsIn(subComments.stream().map(c -> c.getUid()).collect(Collectors.toList()))
                .stream().collect(Collectors.toMap(User::getUid, Function.identity()));


        return makeCommentVO(owner, comment,
                subComments.stream().map(c -> makeCommentVO(userMap.get(c.getUid()), c, null))
                        .collect(Collectors.toList()));
    }

    private CommentVO makeCommentVO(User owner, Comment comment, List<CommentVO> subComments) {
        List<Likes> likes = likeDao.findAllByTargetTypeAndTargetId(Likes.TargetType.COMMENT, comment.getId());
        List<String> likeUsersIds = likes.stream().map(like -> like.getUid()).collect(Collectors.toList());
        List<User> likeUsers = userDao.findAllByUidIsIn(likeUsersIds);

        return CommentVO.from(owner, comment, likeUsers, subComments);
    }


    @Transactional
    public void deleteComment(String uid, int commentId) {
        Comment comment = checkDBComment(uid, commentId);
        comment.setMessage("");
        comment.setDeleted(true);
        commentDao.save(comment);


//        likeDao.deleteAllByTargetTypeAndTargetId(Likes.TargetType.COMMENT, commentId);
//        commentDao.deleteAllByFeedIdAndParentId(comment.getFeedId(), commentId);
//        commentDao.delete(comment);
    }

    public Comment checkDBComment(String owner, int commentId) {
        Comment comment = commentDao.findById(commentId).orElse(null);
        if (comment == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "comment is not exists");
        if (owner != null && !comment.getUid().equals(owner))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);
        return comment;
    }
}
