package com.secondspace.saffy.service;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import com.secondspace.saffy.dao.FeedLogDao;
import com.secondspace.saffy.dao.KeywordLogDao;
import com.secondspace.saffy.dao.UserStatisticsDao;
import com.secondspace.saffy.vo.model.FeedLog;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.user.UserStatisticsVO;

import java.util.List;

@Service
@Slf4j
public class StatisticsService {
    private final UserStatisticsDao userStatisticsDao;
    private final KeywordLogDao keywordLogDao;
    private final FeedLogDao feedLogDao;

    public StatisticsService(UserStatisticsDao userStatisticsDao, KeywordLogDao keywordLogDao, FeedLogDao feedLogDao) {
        this.userStatisticsDao = userStatisticsDao;
        this.keywordLogDao = keywordLogDao;
        this.feedLogDao = feedLogDao;
    }


    public UserStatisticsVO getUserStatistics(String uid) {
        UserStatisticsVO result = userStatisticsDao.getUserStatistics(uid);
        return result;
    }

    public List<String> getTopKeywords(PageableRequestParam pageParam) {
        return keywordLogDao.getTopKeywordsByWeek(pageParam.getPageStartOffset(), pageParam.getLimit());
    }

    public void updateFeedStatisticsCount(int feedId, FeedLog.Type type) {
        feedLogDao.save(FeedLog.builder().feedId(feedId).type(type).build());
    }
}
