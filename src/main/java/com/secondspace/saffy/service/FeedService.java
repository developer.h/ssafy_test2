package com.secondspace.saffy.service;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secondspace.saffy.dao.ArticleDao;
import com.secondspace.saffy.dao.CommentDao;
import com.secondspace.saffy.dao.FeedDao;
import com.secondspace.saffy.dao.KeywordLogDao;
import com.secondspace.saffy.dao.LikeDao;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.feed.FeedVO;
import com.secondspace.saffy.vo.model.Article;
import com.secondspace.saffy.vo.model.Feed;
import com.secondspace.saffy.vo.model.KeywordLog;
import com.secondspace.saffy.vo.model.Likes;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FeedService {
    private final FeedDao feedDao;
    private final ArticleDao articleDao;
    private final CommentDao commentDao;
    private final LikeDao likeDao;
    private final UserDao userDao;
    private final KeywordLogDao keywordLogDao;

    public FeedService(ArticleDao articleDao, FeedDao feedDao, CommentDao commentDao, LikeDao likeDao, UserDao userDao, KeywordLogDao keywordLogDao) {
        this.feedDao = feedDao;
        this.articleDao = articleDao;
        this.commentDao = commentDao;
        this.likeDao = likeDao;
        this.userDao = userDao;
        this.keywordLogDao = keywordLogDao;
    }

    @Transactional
    public FeedVO createFeed(User user, int articleId, String message) {
        Article article = checkDBArticle(user.getUid(), articleId);

        Feed feed = Feed.builder()
                .uid(user.getUid())
                .articleId(article.getId())
                .message(message)
                .build();
        feedDao.save(feed);

        article.setPosted(true);
        articleDao.save(article);

        FeedVO result = makeFeedVO(user, feed, article);
        return result;
    }

    @Transactional
    public FeedVO scrapFeed(User user, String owner, int feedId, String message) {
        Feed orgFeed = checkDBFeed(owner, feedId);
        orgFeed.increaseScrapCount();
        feedDao.save(orgFeed);
        Article orgArticle = checkDBArticle(owner, orgFeed.getArticleId());

        Article copiedArticle = articleDao.save(Article.builder()
                .uid(user.getUid())
                .status(Article.Status.SCRAP)
                .url(orgArticle.getUrl())
                .title(orgArticle.getTitle())
                .mainImage(orgArticle.getMainImage())
                .description(orgArticle.getDescription())
                .isPosted(true)
                .build());

        Feed feed = Feed.builder()
                .uid(user.getUid())
                .articleId(copiedArticle.getId())
                .message(message)
                .byScrap(true)
                .build();
        feedDao.save(feed);
        FeedVO result = makeFeedVO(user, feed, copiedArticle);
        return result;
    }

    public FeedVO getFeed(User user, int feedId) {
        Feed feed = checkDBFeed(user.getUid(), feedId);

        return makeFeedVO(user, feed);
    }

    public List<FeedVO> getAllFeedsByUid(User user) {

        return makeFeedVOResults(user, feedDao.findAllByUidOrderByIdDesc(user.getUid()));
    }

    public List<FeedVO> findUserLikedFeeds(User user, PageableRequestParam pageParam) {
        return makeFeedVOResults(null, feedDao.findUserLikedFeeds(user.getUid(), pageParam.getPageStartToken(Integer.MAX_VALUE), pageParam.getLimit()));
    }

    public List<FeedVO> searchFeedsByKeyword(User user, String keyword, PageableRequestParam pageParam) {
        keywordLogDao.save(KeywordLog.builder()
                .keyword(keyword)
                .type(KeywordLog.Type.SEARCH)
                .build());
        return makeFeedVOResults(null, feedDao.searchFeedsByKeyword(keyword,
                pageParam.getPageStartToken(Integer.MAX_VALUE), pageParam.getLimit()));
    }

    public List<FeedVO> getTopFeeds(PageableRequestParam pageParam) {
        return makeFeedVOResults(null, feedDao.getTopFeeds(
                pageParam.getPageStartOffset(), pageParam.getLimit()));
    }

    private List<FeedVO> makeFeedVOResults(User user, List<Feed> input) {
        if (user != null) {
            return input.stream()
                    .map(feed -> makeFeedVO(user, feed)).collect(Collectors.toList());
        } else {
            Map<String, User> uidUserMap = userDao.findAllByUidIsIn(
                    input.stream().map(f -> f.getUid()).collect(Collectors.toList()))

                    .stream().collect(Collectors.toMap(User::getUid, Function.identity()));
            return input.stream()
                    .map(feed -> makeFeedVO(uidUserMap.get(feed.getUid()), feed)).collect(Collectors.toList());
        }
    }

    private FeedVO makeFeedVO(User user, Feed feed, Article article) {
        if (article == null)
            article = articleDao.findById(feed.getArticleId()).orElse(null);

        List<Likes> likes = likeDao.findAllByTargetTypeAndTargetId(Likes.TargetType.FEED, feed.getId());
        List<String> likeUsersIds = likes.stream().map(like -> like.getUid()).collect(Collectors.toList());
        List<User> likeUsers = userDao.findAllByUidIsIn(likeUsersIds);
        int commentCount = commentDao.getCommentCountByFeedId(feed.getId());

        return FeedVO.from(user, feed, article, likeUsers, commentCount);
    }

    private FeedVO makeFeedVO(User user, Feed feed) {
        return makeFeedVO(user, feed, null);
    }

    @Transactional
    public void deleteFeed(String uid, int feedId) {
        Feed feed = checkDBFeed(uid, feedId);

        likeDao.deleteAllByTargetTypeAndTargetId(Likes.TargetType.FEED, feedId);
        commentDao.deleteAllByFeedId(feedId);
        feedDao.delete(feed);
    }

    public Feed checkDBFeed(String owner, int feedId) {
        Feed feed = feedDao.findById(feedId).orElse(null);
        if (feed == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "feed is not exists");
        if (owner != null && !feed.getUid().equals(owner))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);
        return feed;
    }

    public Article checkDBArticle(String owner, int articleId) {
        Article article = articleDao.findById(articleId).orElse(null);
        if (article == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "article is not exists");
        if (owner != null && !article.getUid().equals(owner))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);
        return article;
    }

}
