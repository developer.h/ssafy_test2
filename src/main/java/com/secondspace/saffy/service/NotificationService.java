package com.secondspace.saffy.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.secondspace.saffy.dao.NotiSettingDao;
import com.secondspace.saffy.dao.NotificationBadgeDao;
import com.secondspace.saffy.dao.NotificationDao;
import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.util.CommonUtil;
import com.secondspace.saffy.vo.follow.FollowVO;
import com.secondspace.saffy.vo.model.NotiSetting;
import com.secondspace.saffy.vo.model.Notification;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.vo.user.NotificationVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class NotificationService {
    private final NotiSettingDao notiSettingDao;
    private final NotificationDao notificationDao;
    private final UserDao userDao;

    private final NotificationBadgeDao notificationBadgeDao;

    public NotificationService(NotiSettingDao notiSettingDao, NotificationDao notificationDao, UserDao userDao, NotificationBadgeDao notificationBadgeDao) {
        this.notiSettingDao = notiSettingDao;
        this.notificationDao = notificationDao;
        this.userDao = userDao;
        this.notificationBadgeDao = notificationBadgeDao;
    }

    public List<NotificationVO> getNotifications(String uid, PageableRequestParam pageParam) {
        List<Notification> notiList = notificationDao.findAllByUidOrderByIdDesc(uid,
                pageParam.getPageStartToken(Integer.MAX_VALUE), pageParam.getLimit());
        List<String> senderUids = notiList.stream().map(n -> n.getSenderUid()).collect(Collectors.toList());
        Map<String, User> senderUidMap = userDao.findAllById(senderUids).stream()
                .collect(Collectors.toMap(User::getUid, Function.identity()));

        return notiList.stream().map(n -> {
            try {
                return NotificationVO.from(n, senderUidMap.get(n.getSenderUid()));
            } catch (JsonProcessingException e) {
                throw new SaffyException(RCode.INTERNAL_SERVER_ERRPR, e);
            }
        }).collect(Collectors.toList());
    }

    public NotiSetting getNotificationSetting(String uid) {
        NotiSetting notiSetting = notiSettingDao.findByUid(uid);
        return notiSetting != null ? notiSetting : NotiSetting.DEFAULT_NOTI_SETTING;
    }

    public void sendNotification(User from, List<FollowVO> to, Notification.Type notiType, Object data) {
        if (CommonUtil.isValid(to)) {
            for (FollowVO target : to) {
                sendNotification(from, target.getUid(), notiType, data);
            }
        }
    }

    public void sendNotification(User from, String to, Notification.Type notiType, Object data) {
        try {
            if (checkNotiSetting(to, notiType)) {
                notificationDao.save(Notification.builder()
                        .uid(to)
                        .senderUid(from.getUid())
                        .notiType(notiType)
                        .data(CommonUtil.convertObjectToString(data))
                        .build());

                notificationBadgeDao.updateBadgeCount(to, 1);
            }
        } catch (Exception e) {
            throw new SaffyException(RCode.INTERNAL_SERVER_ERROR, e);
        }
    }


    private boolean checkNotiSetting(String uid, Notification.Type notiType) {
        NotiSetting notiSetting = getNotificationSetting(uid);

        switch (notiType) {
            case followerNewFeed:
                return notiSetting.isFollowerNewFeed();
            case myFeedLike:
                return notiSetting.isMyFeedLike();
            case myFeedComment:
                return notiSetting.isMyFeedComment();
            case myFeedScrap:
                return notiSetting.isMyFeedScrap();
            case newFollowerAcceptResult:
                return notiSetting.isNewFollowerAcceptResult();
            case newFollower:
                return notiSetting.isNewFollower();
            case newFollowerAccept:
                return notiSetting.isNewFollowerAccept();
            case cancelFollower:
                return notiSetting.isCancelFollower();
        }
        return false;
    }
}
