package com.secondspace.saffy.service;

import com.secondspace.saffy.dao.ArticleDao;
import com.secondspace.saffy.dao.ArticleKeywordDao;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.feed.ArticleRequestVO;
import com.secondspace.saffy.vo.feed.ArticleVO;
import com.secondspace.saffy.vo.model.Article;
import com.secondspace.saffy.vo.model.ArticleKeyword;
import com.secondspace.saffy.vo.response.PageableRequestParam;
import com.secondspace.saffy.vo.response.RCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ArticleService {
    private final ArticleDao articleDao;
    private final ArticleKeywordDao articleKeywordDao;

    public ArticleService(ArticleDao articleDao, ArticleKeywordDao articleKeywordDao) {
        this.articleDao = articleDao;
        this.articleKeywordDao = articleKeywordDao;
    }

    public ArticleVO createArticle(String uid, ArticleRequestVO request) {
        Article article = articleDao.save(
                Article.builder()
                        .uid(uid)
                        .url(request.getUrl())
                        .title(request.getTitle())
                        .description(request.getDescription())
                        .status(request.getStatus())
                        .mainImage(request.getMainImage())
                        .build()
        );

        List<ArticleKeyword> keywords = request.getKeywords().stream()
                .map(k -> ArticleKeyword.builder()
                        .articleId(article.getId())
                        .keyword(k).build()).collect(Collectors.toList());

        articleKeywordDao.saveAll(keywords);

        return ArticleVO.from(article);
    }

    public Article getArticle(int articleId) {
        return articleDao.findById(articleId).orElse(null);
    }

    public List<ArticleVO> getArticlesByStatus(String uid, Article.Status status, PageableRequestParam pageParam) {
        if (status == Article.Status.SKIP)
            return articleDao.findAllSkippedArticleByUid(uid, pageParam.getPageStartToken(Integer.MAX_VALUE), pageParam.getLimit())
                    .stream().map(a -> ArticleVO.from(a)).collect(Collectors.toList());
        else
            return articleDao.findAllByUidAndStatus(uid, status.getValue(), pageParam.getPageStartToken(Integer.MAX_VALUE), pageParam.getLimit())
                    .stream().map(a -> ArticleVO.from(a)).collect(Collectors.toList());

    }

    public void modifyArticleStatus(String uid, int articleId, Article.Status status) {
        Article article = articleDao.findById(articleId).orElse(null);
        if (article == null)
            throw new SaffyException(RCode.RESOURCE_NOT_EXISTS, "article is not exists");
        if (!article.getUid().equals(uid))
            throw new SaffyException(RCode.DO_NOT_HAVE_PERMISSION);

        Article.Status prev = article.getStatus();
        log.info("modifyArticleStatus - uid : " + uid + ", status : " + prev + " -> " + status);
        article.setStatus(status);
        articleDao.save(article);
    }

    @Transactional
    public void deleteArticle(String uid, int articleId) {
        articleKeywordDao.deleteAllByArticleId(articleId);
        articleDao.deleteByIdAndUid(articleId, uid);
    }
}
