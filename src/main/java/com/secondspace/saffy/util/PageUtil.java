package com.secondspace.saffy.util;

import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.response.PageToken;
import com.secondspace.saffy.vo.response.RCode;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

public class PageUtil {
    private static final ConcurrentHashMap<Class, Field> PAGE_FIELD_MAP = new ConcurrentHashMap<>();

    public static String getNextPageToken(Object obj) {
        Field pageTokenField = PAGE_FIELD_MAP.get(obj.getClass());
        if (pageTokenField == null) {
            for (Field f : obj.getClass().getDeclaredFields()) {
                if (f.getDeclaredAnnotation(PageToken.class) != null) {
                    PAGE_FIELD_MAP.put(obj.getClass(), f);
                    pageTokenField = f;
                    break;
                }
            }
        }


        if (pageTokenField == null) {
            throw new SaffyException(RCode.INTERNAL_SERVER_ERRPR, "no page token field in " + obj.getClass());
        }

        try {
            pageTokenField.setAccessible(true);
            String result = pageTokenField.get(obj).toString();
            return result;
        } catch (IllegalAccessException e) {
            throw new SaffyException(RCode.INTERNAL_SERVER_ERRPR, "error to get page token value in " + obj.getClass(), e);
        }
    }
}
