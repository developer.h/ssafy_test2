package com.secondspace.saffy.util;

import com.secondspace.saffy.constant.AppConstants;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Random;

public class UidUtil {
    public static String generateUid() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 8;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedUid = buffer.toString();
        return generatedUid;
    }

    public static String getUidFromContext(RequestAttributes attributes) {
        if(attributes != null)
            return (String) RequestContextHolder.getRequestAttributes().getAttribute(AppConstants.userUid, RequestAttributes.SCOPE_REQUEST);
        else
            return null;
    }
}
