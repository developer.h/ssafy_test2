package com.secondspace.saffy.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collection;

public class CommonUtil {

    public static boolean isValid(String str) {
        return str != null && str.length() > 0;
    }

    public static boolean isValid(Collection collection) {
        return collection != null && collection.size() > 0;
    }

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static String convertObjectToString(Object obj) throws JsonProcessingException {
        return MAPPER.writeValueAsString(obj);
    }
}
