package com.secondspace.saffy.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    PARAMETER_REQUIRED (1001, "Parameter '%s' required", HttpStatus.BAD_REQUEST),
    HEADER_REQUIRED (1002, "Header required", HttpStatus.BAD_REQUEST),
    BAD_CREDENTIAL (1003, "Bad Credential", HttpStatus.BAD_REQUEST),
    UNAUTHORIZED (2001, "Unauthorized request", HttpStatus.UNAUTHORIZED),
    CANNOT_FIND_USER (2002, "Cannot find user '%s'", HttpStatus.UNAUTHORIZED),
    DATABASE_ERROR (5001, "Database Error", HttpStatus.INTERNAL_SERVER_ERROR);

    private final int errorCode;
    private final HttpStatus httpStatus;
    private final String errorMessage;

    public int getErrorCode() {
        return errorCode;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    ErrorCode(int errorCode, String errorMessage, HttpStatus status) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.httpStatus = status;
    }


}
