package com.secondspace.saffy.exception;

import com.secondspace.saffy.vo.response.RCode;

public class SaffyException extends RuntimeException {
    RCode errorCode;
    String errorMsgParameter;

    String errorMessage;

    public SaffyException(RCode appErrorCode, String errorMsgParameter, Throwable t) {
        super(t);
        this.errorCode = appErrorCode;
        this.errorMsgParameter = errorMsgParameter;
    }

    public SaffyException(RCode appErrorCode, String errorMsgParameter) {
        this.errorCode = appErrorCode;
        this.errorMsgParameter = errorMsgParameter;
    }

    public SaffyException(RCode appErrorCode, Throwable t) {
        super(t);
        this.errorCode = appErrorCode;
    }

    public SaffyException(RCode appErrorCode) {
        this.errorCode = appErrorCode;
    }


    public RCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(RCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsgParameter() {
        return errorMsgParameter;
    }

    public void setErrorMsgParameter(String errorMsgParameter) {
        this.errorMsgParameter = errorMsgParameter;
    }

    public String getErrorMessage() {
        if(errorMsgParameter != null)
            return String.format(errorCode.getResultMessage(), errorMsgParameter);

        return errorCode.getResultMessage();
    }

    public RCode getAppErrorCode() {
        return errorCode;
    }

    public void setAppErrorCode(RCode appErrorCode) {
        this.errorCode = appErrorCode;
    }


}
