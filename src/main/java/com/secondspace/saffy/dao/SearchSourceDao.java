package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.SearchSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SearchSourceDao extends JpaRepository<SearchSource, Integer> {
    @Query(nativeQuery = true,
            value="select * from search_source where uid = :uid " +
                    "and id >= :pageStartToken order by id limit :limit")
    List<SearchSource> findAllByUid(String uid, String pageStartToken, int limit);
}
