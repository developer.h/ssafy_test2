package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.SearchHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SearchHistoryDao extends JpaRepository<SearchHistory, Integer> {
    List<SearchHistory> findAllByUidOrderByIdDesc(String uid);

    void deleteAllByUid(String uid);

    void deleteAllByUidAndResultUidIsNotNullAndKeywordIsNull(String uid);

    void deleteAllByUidAndResultUidIsNullAndKeywordIsNotNull(String uid);
}
