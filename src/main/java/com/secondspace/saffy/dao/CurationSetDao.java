package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.CurationSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CurationSetDao extends JpaRepository<CurationSet, Integer> {
    @Query(nativeQuery = true,
    value = "select * from curation_set where uid = :uid " +
            "and id >= :pageStartToken order by id limit :limit")
    List<CurationSet> findAllByUid(String uid, String pageStartToken, int limit);
}
