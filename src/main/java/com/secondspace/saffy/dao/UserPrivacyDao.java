package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.UserPrivacy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPrivacyDao extends JpaRepository<UserPrivacy, String> {
    UserPrivacy findByUid(String uid);
}
