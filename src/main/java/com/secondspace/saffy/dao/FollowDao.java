package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.Follow;
import com.secondspace.saffy.vo.response.ListItemResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

public interface FollowDao extends JpaRepository<Follow, String> {
    ArrayList<Follow> getAllByTargetUid(String targetUid);
    ArrayList<Follow> getAllByUid(String uid);
    Follow getByUidAndTargetUid(String uid, String targetUid);
    ArrayList<Follow> getAllByTargetUidAndStatus(String targetUid, Follow.Status status);
    ArrayList<Follow> getAllByUidAndStatus(String targetUid, Follow.Status status);
    Follow getByUidAndTargetUidAndStatus(String uid, String targetUid, Follow.Status status);

    @Query(nativeQuery = true,
            value = "SELECT * FROM follow " +
                    "WHERE uid = :uid AND status = :status " +
                    "AND approval_date <= :approvalDate ORDER BY approval_date DESC limit :limit")
    ArrayList<Follow> getAllByUidAndStatusForPaging(@Param("uid")String uid,
                                                    @Param("status")int status,
                                                    @Param("approvalDate")String approvalDate,
                                                    @Param("limit")int limit);

    @Query(nativeQuery = true,
            value = "SELECT * FROM follow " +
                    "WHERE target_uid = :targetUid AND status = :status " +
                    "AND approval_date <= :approvalDate ORDER BY approval_date DESC limit :limit")
    ArrayList<Follow> getAllByTargetUidAndStatusForPaging(@Param("targetUid")String targetUid,
                                                    @Param("status")int status,
                                                    @Param("approvalDate")String approvalDate,
                                                    @Param("limit")int limit);

    int countByUidAndStatus(String uid, Follow.Status status);
    int countByTargetUidAndStatus(String targetId, Follow.Status status);
}
