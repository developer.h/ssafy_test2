package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.UserEmailValidation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserEmailValidationDao extends JpaRepository<UserEmailValidation, String> {
    UserEmailValidation findByCode(String code);
}
