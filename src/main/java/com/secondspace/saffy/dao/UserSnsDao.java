package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.model.UserSns;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSnsDao extends JpaRepository<UserSns, Integer> {
    UserSns getByUid(String uid);
    UserSns getBySnsUid(String snsUid);
}
