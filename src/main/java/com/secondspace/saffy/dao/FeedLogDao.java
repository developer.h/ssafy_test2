package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.FeedLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedLogDao extends JpaRepository<FeedLog, Integer> {

}
