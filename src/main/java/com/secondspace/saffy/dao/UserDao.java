package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface UserDao extends JpaRepository<User, String> {
    User getUserByEmail(String email);

    User getUserByNickname(String nickName);

    List<User> findAllByUidIsIn(Collection<String> uids);

    @Query(nativeQuery = true,
            value = "SELECT * FROM user WHERE uid LIKE  %:keyword% OR nickname LIKE %:keyword% OR email LIKE  %:keyword%")
    List<User> searchUsersByKeyword(@Param("keyword") String keyword);
}
