package com.secondspace.saffy.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.secondspace.saffy.vo.model.CurationSource;

import java.util.List;

public interface CurationSourceDao extends JpaRepository<CurationSource, Integer> {
    List<CurationSource> findAllByCurationId(int curationId);

    void deleteAllByCurationId(int curationId);

    void deleteByCurationIdAndSearchSourceId(int curationId, int searchSourceId);

    void deleteAllBySearchSourceId(int searchSourceId);
}
