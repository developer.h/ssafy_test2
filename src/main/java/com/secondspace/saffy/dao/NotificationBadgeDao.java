package com.secondspace.saffy.dao;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.response.RCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
@Slf4j
public class NotificationBadgeDao {

    private final Firestore firestore;

    private static final String NAME_COLLECTION_NOTIFICATION_BADGE = "notificationBadge";
    private static final String NAME_FIELD_NOTIFICATION_BADGE_COUNT = "badgeCount";


    public NotificationBadgeDao(Firestore firestore) {
        this.firestore = firestore;
    }

    public void updateBadgeCount(String uid, int count) {
        try {
            DocumentReference docRef = firestore.collection(NAME_COLLECTION_NOTIFICATION_BADGE).document(uid);
            ApiFuture<WriteResult> result = null;

            if (docRef.get().get().contains(NAME_FIELD_NOTIFICATION_BADGE_COUNT)) {
                result = docRef.update(NAME_FIELD_NOTIFICATION_BADGE_COUNT, FieldValue.increment(count));
            } else {
                Map<String, Object> data = new HashMap<>();
                data.put(NAME_FIELD_NOTIFICATION_BADGE_COUNT, count);
                result = docRef.set(data);
            }

            log.info("sendBadgeCount result - uid : " + uid + ", " + result.get().getUpdateTime());
        } catch (Exception e) {
            throw new SaffyException(RCode.INTERNAL_SERVER_ERRPR, "firestore error", e);
        }
    }
}
