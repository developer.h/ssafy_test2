package com.secondspace.saffy.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.secondspace.saffy.vo.model.KeywordLog;

import java.util.List;

public interface KeywordLogDao extends JpaRepository<KeywordLog, Integer> {
    @Query(nativeQuery = true,
            value = "select * from (" +
                    "SELECT " +
                    "keyword " +
                    "FROM keyword_log " +
                    "WHERE create_date > DATE_SUB(NOW(), INTERVAL 7 DAY) " +
                    "GROUP BY keyword " +
                    "ORDER BY COUNT(*) DESC" +
                    " ) a limit :pageStartOffset, :limit"
    )
    List<String> getTopKeywordsByWeek(int pageStartOffset, int limit);
}
