package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.ArticleKeyword;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleKeywordDao extends JpaRepository<ArticleKeyword, Integer> {
    void deleteAllByArticleId(int articleId);
}
