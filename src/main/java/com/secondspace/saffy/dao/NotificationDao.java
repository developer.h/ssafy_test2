package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NotificationDao extends JpaRepository<Notification, Integer> {
    @Query(nativeQuery = true,
    value="select * from notification where uid = :uid " +
            "and id <= :pageStartToken order by id desc limit :limit")
    List<Notification> findAllByUidOrderByIdDesc(String uid, String pageStartToken, int limit);
}
