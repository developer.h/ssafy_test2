package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.user.UserStatisticsVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserStatisticsDao extends JpaRepository<UserStatisticsVO, String> {

    @Query(nativeQuery = true,
            value = "SELECT " +
                    " :uid AS uid, " +
                    "(SELECT COUNT(id) FROM follow WHERE uid = :uid and status = 1) AS follow, " +
                    "(SELECT COUNT(id) FROM follow WHERE target_uid = :uid and status = 1) AS follower, " +
                    " " +
                    "(SELECT COUNT(id) FROM feed WHERE uid = :uid) AS feed, " +
                    "(SELECT COUNT(id) FROM curation_set WHERE uid = :uid) AS curation_set, " +
                    "(SELECT IFNULL(SUM(scrap_count),0) FROM feed WHERE uid = :uid) AS scrap_in, " +
                    "(SELECT COUNT(id) FROM feed WHERE uid = :uid AND by_scrap = 1) AS scrap_out, " +
                    "(SELECT COUNT(id) FROM `likes` WHERE target_uid = :uid) AS like_in, " +
                    "(SELECT COUNT(id) FROM `likes` WHERE uid = :uid) AS like_out, " +
                    "(SELECT COUNT(id) FROM `comment` WHERE target_uid = :uid) AS comment_in, " +
                    "(SELECT COUNT(id) FROM `comment` WHERE uid = :uid) AS comment_out "
    )
    UserStatisticsVO getUserStatistics(@Param("uid") String uid);
}
