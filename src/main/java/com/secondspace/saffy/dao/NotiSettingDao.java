package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.NotiSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotiSettingDao extends JpaRepository<NotiSetting, Integer> {
    NotiSetting findByUid(String uid);
}
