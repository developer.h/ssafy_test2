package com.secondspace.saffy.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.secondspace.saffy.vo.model.CurationKeyword;

import java.util.List;

public interface CurationKeywordDao extends JpaRepository<CurationKeyword, Integer> {
    List<CurationKeyword> findAllByCurationId(int curationId);

    void deleteByCurationIdAndKeyword(int curationId, String keyword);

    void deleteAllByCurationId(int curationSetId);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT(keyword) AS keyword FROM curation_keyword WHERE keyword like %:keyword% " +
                    " order by keyword")
    List<String> searchKeywords(@Param("keyword") String keyword);
}
