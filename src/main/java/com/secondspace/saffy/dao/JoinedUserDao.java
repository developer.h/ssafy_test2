package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.JoinedUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JoinedUserDao extends JpaRepository<JoinedUser, String> {
    @Query(nativeQuery =true,
            value = "SELECT *  FROM user " +
                    "LEFT JOIN user_sns on user.uid = user_sns.uid " +
                    "WHERE user.uid IN (:uids)")
    List<JoinedUser> getAllByUids(@Param("uids") List<String> uids);
}
