package com.secondspace.saffy.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.secondspace.saffy.vo.model.Likes;

import java.util.List;
import java.util.Optional;

public interface LikeDao extends JpaRepository<Likes, Integer> {

    List<Likes> findAllByTargetTypeAndTargetId(Likes.TargetType type, int targetId);

    Optional<Likes> findByUidAndTargetTypeAndTargetId(String uid, Likes.TargetType type, int targetId);

    void deleteByUidAndTargetTypeAndTargetId(String uid, Likes.TargetType type, int targetId);

    void deleteAllByTargetTypeAndTargetId(Likes.TargetType type, int targetId);
}
