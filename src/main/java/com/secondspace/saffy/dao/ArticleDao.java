package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ArticleDao extends JpaRepository<Article, Integer> {
//    List<Article> findAllByUidAndStatus(String uid, int status, PageableRequestParam param);

    @Query(nativeQuery = true,
            value = "select * from article where uid=:uid and status = :status " +
                    "and id <= :startToken order by id desc limit :limit ")
    List<Article> findAllByUidAndStatus(String uid, int status, String startToken, int limit);

    @Query(nativeQuery = true,
            value = "select * from article where uid=:uid and status = 1 " +
                    "and update_date >= DATE_SUB(NOW(), INTERVAL 1 DAY) " +
                    "and id <= :startToken order by id desc limit :limit ")
    List<Article> findAllSkippedArticleByUid(String uid, String startToken, int limit);

    void deleteByIdAndUid(int articleId, String uid);

    boolean existsByUidAndUrl(String uid, String url);
}
