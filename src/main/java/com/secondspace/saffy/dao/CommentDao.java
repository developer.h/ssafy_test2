package com.secondspace.saffy.dao;

import com.secondspace.saffy.vo.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentDao extends JpaRepository<Comment, Integer> {
    @Query(nativeQuery = true,
            value = "select * from comment where feed_id = :feedId and id <= :pageStartToken order by id desc limit :limit")
    List<Comment> findAllByFeedIdOrderByIdDesc(int feedId, String pageStartToken, int limit);

    void deleteAllByFeedId(int feedId);

    void deleteAllByFeedIdAndParentId(int feedId, int parentId);

    @Query(nativeQuery = true,
            value = "select count(id) as cnt from comment where feed_id = :feedId")
    int getCommentCountByFeedId(int feedId);

    List<Comment> findAllByParentIdOrderByIdDesc(int parentId);
}
