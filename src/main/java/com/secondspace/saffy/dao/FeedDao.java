package com.secondspace.saffy.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.secondspace.saffy.vo.model.Feed;

import java.util.List;

public interface FeedDao extends JpaRepository<Feed, Integer> {
    List<Feed> findAllByUidOrderByIdDesc(String uid);

    @Query(nativeQuery = true,
            value = "SELECT f.* FROM feed f, `likes` l " +
                    "WHERE f.id = l.target_id AND l.uid = :uid AND l.target_type = 0 " +
                    "and f.id <= :pageStartToken ORDER BY f.id DESC limit :limit")
    List<Feed> findUserLikedFeeds(String uid, String pageStartToken, int limit);

    @Query(nativeQuery = true,
            value = "select * from (" +
                    "SELECT  " +
                    "f.* " +
                    "FROM feed f " +
                    "INNER JOIN ( " +
                    "SELECT feed_id,  " +
                    "SUM( IF(`type` = 0, 1, 0)) AS like_cnt, " +
                    "SUM( IF(`type` = 1, 1, 0)) AS view_cnt, " +
                    "SUM( IF(`type` = 2, 1, 0)) AS share_cnt " +
                    "FROM feed_log " +
                    "WHERE " +
                    "create_date > DATE_SUB(NOW(), INTERVAL 7 DAY) " +
                    "GROUP BY feed_id " +
                    ") fl " +
                    " ON f.id = fl.feed_id " +
                    "LEFT OUTER JOIN user_privacy up " +
                    " ON f.uid = up.uid " +
                    "WHERE " +
                    "up.private_mode = 0 OR up.private_mode IS NULL " +
                    "ORDER BY fl.like_cnt DESC, fl.view_cnt DESC, fl.share_cnt DESC" +
                    " ) a limit :pageStartOffset, :limit"
    )
    List<Feed> getTopFeeds(int pageStartOffset, int limit);

    @Query(nativeQuery = true,
            value = "SELECT f.* " +
                    "FROM  " +
                    "feed f " +
                    "INNER JOIN article_keyword ak " +
                    " ON f.article_id = ak.article_id " +
                    "LEFT OUTER JOIN user_privacy up " +
                    " ON f.uid = up.uid " +
                    "WHERE " +
                    " ak.keyword = :keyword " +
                    " AND (up.private_mode = 0 OR up.private_mode IS NULL) " +
                    " AND f.id <= :pageStartToken " +
                    "ORDER BY f.id DESC " +
                    "LIMIT :limit")
    List<Feed> searchFeedsByKeyword(String keyword, String pageStartToken, int limit);


}
