package com.secondspace.saffy.auth;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.service.UserService;
import com.secondspace.saffy.vo.response.RCode;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.vo.model.User;
import io.jsonwebtoken.Claims;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.thymeleaf.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class JwtRequestFilter extends OncePerRequestFilter {


    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    @Autowired
    private UserService userService;

    private final JwtToken jwtTokenUtil;

    public JwtRequestFilter(JwtToken jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        AntPathMatcher matcher = new AntPathMatcher();
        if(HttpMethod.OPTIONS.matches(request.getMethod()))
            return true;
        if (!matcher.match("/account/**", request.getServletPath()))
            return false;
        if (matcher.match("/user/profileImage", request.getServletPath()))
            return true;
        if (HttpMethod.PUT.matches(request.getMethod()) &&
                matcher.match("/account/password",request.getServletPath()))
            return true;
        if (matcher.match("/account/password/changeRequest",request.getServletPath()))
            return true;

        return true;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        final String requestTokenHeader = request.getHeader("Authorization");

        String userName = null;
        String jwtToken = null;
        String socialType;


        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                Claims claims = jwtTokenUtil.getAllClaimsFromToken(jwtToken);
                userName = claims.getSubject();
                socialType = (String) claims.get("test");
                if (socialType != null) {

                }
            } catch (Exception e) {

            }

        } else {
            logger.warn("JWT Token does not begin with Bearer String");
        }


        if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(userName);
            if(!jwtTokenUtil.validateTokenUser(jwtToken, userDetails)) {

            } else if(!jwtTokenUtil.validateTokenExpireTime(jwtToken)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("rCode", RCode.TOKEN_EXPIRED.getResultCode());
                jsonObject.put("rMsg", RCode.TOKEN_EXPIRED.getResultMessage());
                jsonObject.put("status", RCode.TOKEN_EXPIRED.getHttpStatus().value());

                request.setAttribute("RCode", jsonObject);
            } else {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                RequestContextHolder.getRequestAttributes().setAttribute(AppConstants.userUid, userName, RequestAttributes.SCOPE_REQUEST);
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        chain.doFilter(request, response);
    }
}
