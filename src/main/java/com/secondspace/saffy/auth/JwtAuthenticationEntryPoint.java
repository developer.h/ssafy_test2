package com.secondspace.saffy.auth;

import com.secondspace.saffy.vo.response.RCode;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException
    {
        JSONObject jsonObject = (JSONObject) request.getAttribute("RCode");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        PrintWriter out = response.getWriter();
        if(jsonObject == null) {
            jsonObject = new JSONObject();
            jsonObject.put("rCode", RCode.UNAUTHORIZED.getResultCode());
            jsonObject.put("rMsg", RCode.UNAUTHORIZED.getResultMessage());
        }
        out.print(jsonObject);
    }
}
