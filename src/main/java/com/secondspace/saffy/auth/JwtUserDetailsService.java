package com.secondspace.saffy.auth;

import com.secondspace.saffy.dao.UserDao;
import com.secondspace.saffy.dao.UserSnsDao;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.model.UserSns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    UserDao userDao;

    @Autowired
    UserSnsDao userSnsDao;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        String encodedPassword;
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        try {
            Optional<User> userOptional = userDao.findById(userName);

            if (userOptional.isPresent() != true) {
                throw new UsernameNotFoundException(userName);
            }
            User user = userOptional.get();

            UserSns userSns = userSnsDao.getByUid(user.getUid());
            if (userSns != null) {
                encodedPassword = encoder.encode("");
            } else {
                encodedPassword = encoder.encode(user.getPassword());
            }

        } catch (Exception e) {
            throw new UsernameNotFoundException(userName);
        }

        return new org.springframework.security.core.userdetails.User(userName, encodedPassword, new ArrayList<>());
    }
}
