package com.secondspace.saffy.mapper;

import com.secondspace.saffy.vo.db.UserTable;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    @Select("SELECT * FROM user WHERE email = #{email}")
    UserTable selectUserByEmail(@Param("email")String email);

    @Select("SELECT * FROM user WHERE uid = #{uid}")
    UserTable selectUserByUid(@Param("uid") String uid);

    @Insert("INSERT INTO user (uid, email, password, nick_name, social_type) " +
            "VALUES (#{uid}, #{email}, #{password}, #{nick_name}, #{social_type})")
    void insertUser(@Param("uid")String uid, @Param("email") String email,
                    @Param("password") String password, @Param("nick_name") String nick_name,
                    @Param("social_type") String social_type);
}
