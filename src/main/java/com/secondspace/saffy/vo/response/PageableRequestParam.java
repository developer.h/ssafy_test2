package com.secondspace.saffy.vo.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import io.swagger.annotations.ApiModelProperty;

@ToString(exclude = {"limit", "isRowNumPage"})
public class PageableRequestParam {
    @Getter
    @Setter
    private Integer count;

    @ApiModelProperty(hidden = true)
    private Integer limit;

    @ApiModelProperty(hidden = true)
    private boolean isRowNumPage;

    @Getter
    @Setter
    private String pageStartToken;


    public int getLimit() {
        if (count == null)
            return Integer.MAX_VALUE;
        return count + 1;
    }

    @ApiModelProperty(hidden = true)
    public int getPageStartOffset() {
        this.isRowNumPage = true;
        if (pageStartToken == null)
            return 0;
        else
            return Integer.parseInt(pageStartToken);
    }

    @ApiModelProperty(hidden = true)
    public boolean isRowNumPage(){
        return this.isRowNumPage;
    }

    public String getPageStartToken(Object defaultValue) {
        if (pageStartToken == null)
            return defaultValue.toString();
        return pageStartToken;
    }


}
