package com.secondspace.saffy.vo.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse {
    @ApiModelProperty(value = "Result Code", position = 1)
    protected int rCode;
    @ApiModelProperty(value = "Result Message", position = 2)
    protected String rMsg;
    public static final BaseResponse OK = new BaseResponse();

    static {
        OK.rCode = RCode.OK.getResultCode();
        OK.rMsg = RCode.OK.getResultMessage();
    }
}
