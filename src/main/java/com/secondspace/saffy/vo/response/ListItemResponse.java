package com.secondspace.saffy.vo.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

import com.secondspace.saffy.util.PageUtil;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListItemResponse<T> extends BaseResponse {

    @ApiModelProperty(value = "Has more data in server", position = 3)
    private boolean hasMore = false;

    @ApiModelProperty(value = "Next page token", position = 4)
    private String nextPageToken;

    @ApiModelProperty(value = "Contents List Data", position = 5)
    private List<T> list;

    private ListItemResponse() {
    }

    public static <T> ListItemResponse<T> create(List<T> list) {
        ListItemResponse response = new ListItemResponse();
        response.rCode = RCode.OK.getResultCode();
        response.rMsg = RCode.OK.getResultMessage();
        response.list = list;
        return response;
    }

    public static <T> ListItemResponse<T> create(RCode resultCode, List<T> list) {
        ListItemResponse response = new ListItemResponse();
        response.rCode = resultCode.getResultCode();
        response.rMsg = resultCode.getResultMessage();
        response.list = list;
        return response;
    }

    public static <T> ListItemResponse<T> create(PageableRequestParam pageParam, List<T> list) {
        ListItemResponse response = new ListItemResponse();
        response.rCode = RCode.OK.getResultCode();
        response.rMsg = RCode.OK.getResultMessage();

        if (list != null && pageParam != null && pageParam.getCount() != null
                && pageParam.getCount() < list.size()) {
            T next = list.get(pageParam.getCount());
            response.hasMore = true;
            if (pageParam.isRowNumPage())
                response.nextPageToken = (pageParam.getPageStartOffset() + pageParam.getCount()) + "";
            else
                response.nextPageToken = PageUtil.getNextPageToken(next);
            response.list = list.subList(0, pageParam.getCount());
        } else {
            response.list = list;
        }

        return response;
    }


}
