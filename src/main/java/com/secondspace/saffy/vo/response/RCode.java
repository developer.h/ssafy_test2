package com.secondspace.saffy.vo.response;

import org.springframework.http.HttpStatus;

public enum RCode {
    OK(100000, "OK", HttpStatus.OK),

    // Common Validation
    HEADER_REQUIRED     (101001, "Parameter '%s' required", HttpStatus.BAD_REQUEST),
    HEADER_INVALID      (101002, "Header '%s' invalid", HttpStatus.BAD_REQUEST),
    PARAMETER_REQUIRED  (101011, "Parameter '%s' required", HttpStatus.BAD_REQUEST),
    PARAMETER_INVALID   (101012, "Parameter '%s' invalid", HttpStatus.BAD_REQUEST),
    BODY_REQUIRED       (101021, "Body '%s' required", HttpStatus.BAD_REQUEST),
    BODY_INVALID        (101022, "Body '%s' invalid", HttpStatus.BAD_REQUEST),

    RESOURCE_NOT_EXISTS     (101031, "Resouce not found '%s'", HttpStatus.BAD_REQUEST),
    RESOURCE_OWNER_MISMATCH (101032, "Resouce not found '%s'", HttpStatus.BAD_REQUEST),

    // Server Error
    INTERNAL_SERVER_ERROR   (101500, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR),
    INTERNAL_DATABASE_ERROR (101501, "Internal Database Error", HttpStatus.INTERNAL_SERVER_ERROR),
    REFERENCE_SERVER_ERROR  (101502, "Reference Server Error", HttpStatus.SERVICE_UNAVAILABLE),

    // Account 2
    TOKEN_EXPIRED(102001, "Token is expired", HttpStatus.UNAUTHORIZED),
    UNAUTHORIZED(102002, "Unauthorized", HttpStatus.UNAUTHORIZED),
    ALREADY_JOINED(102003, "Email address '$s' is already joined", HttpStatus.BAD_REQUEST),
    PASSWORD_NOT_MATCHED(102004, "Password is not matched", HttpStatus.BAD_REQUEST),
    NICKNAME_ALREADY_IN_USE(102005, "Nickname is already in use", HttpStatus.BAD_REQUEST),
    INVALID_VALIDATION_CODE(102006, "Validation code is not invalid",HttpStatus.BAD_REQUEST),
    CANNOT_FIND_USER_FOR_PROFILE(102007, "Cannot find user '%s'", HttpStatus.BAD_REQUEST),
    CANNOT_FIND_EMAIL(102008, "Cannot find email '%s'" , HttpStatus.BAD_REQUEST ),
    INVALID_PASSWORD(102009, "Invalid password", HttpStatus.BAD_REQUEST),

    // User 3
    USER_NOT_EXISTS     (103001, "Not exists user", HttpStatus.BAD_REQUEST),

    // Follow 4
    ALREADY_FOLLOWING(104001, "You already follow '%s'", HttpStatus.BAD_REQUEST),
    CANNOT_FIND_USER_FOR_FOLLOWING(104002,"Cannot find user '$s' following" , HttpStatus.BAD_REQUEST),
    CANNOT_FIND_USER_FOR_FOLLOWER_APPROVAL(104003,"Cannot find user '$s' to approve" , HttpStatus.BAD_REQUEST),
    CANNOT_FIND_APPROVAL_REQUEST(104005, "Cannot find request to approve", HttpStatus.BAD_REQUEST),
    CANNOT_FOLLOW_YOURSELF(104006, "Cannot follow yourself", HttpStatus.BAD_REQUEST),

    // Feed 5

    // Comment 6

    // Likes 7

    // Curation 8

    // Article 9

    // Search-Source 10

    BAD_CREDENTIAL(1003, "Bad Credential", HttpStatus.BAD_REQUEST),
    CANNOT_FIND_USER_FOR_AUTH(2002, "Cannot find user '%s'", HttpStatus.UNAUTHORIZED),
    DO_NOT_HAVE_PERMISSION(2003, "Do not have permission for this resource", HttpStatus.BAD_REQUEST),
    DATABASE_ERROR(5001, "Database Error", HttpStatus.INTERNAL_SERVER_ERROR),
    TOKEN_GENERATION_ERROR(5002, "Token generation error", HttpStatus.INTERNAL_SERVER_ERROR),
    INTERNAL_SERVER_ERRPR(5001, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);


    private final int resultCode;
    private final HttpStatus httpStatus;
    private final String resultMessage;

    public int getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    RCode(int errorCode, String errorMessage, HttpStatus status) {
        this.resultCode = errorCode;
        this.resultMessage = errorMessage;
        this.httpStatus = status;
    }


}
