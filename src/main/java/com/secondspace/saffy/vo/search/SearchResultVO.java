package com.secondspace.saffy.vo.search;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.secondspace.saffy.vo.user.ProfileVO;

import java.util.Collection;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchResultVO {
    private String query;

    private List<ProfileVO> users;

    private Collection<String> keywords;
}
