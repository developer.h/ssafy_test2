package com.secondspace.saffy.vo.follow;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.Follow;
import com.secondspace.saffy.vo.response.PageToken;
import com.secondspace.saffy.vo.user.ProfileVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FollowVO {
    String uid;
    String nickname;
    String description;
    String profileImage;
    Boolean isFollowing;
    Boolean isFollower;
    @PageToken
    @JsonIgnore
    String approvalDate;
    Follow.Status approved;

    public static FollowVO from(Follow follow, ProfileVO userProfile) {
        return FollowVO.builder().uid(userProfile.getUid())
                .approved(follow.getStatus())
                .approvalDate(follow.getApprovalDate())
                .nickname(userProfile.getNickname())
                .profileImage(userProfile.getProfileImage())
                .description(userProfile.getDescription()).build();
    }

    public static FollowVO from(Follow follow, Boolean isFollowing, ProfileVO userProfile) {
        return FollowVO.builder().uid(userProfile.getUid())
                .approved(follow.getStatus())
                .isFollowing(isFollowing)
                .approvalDate(follow.getApprovalDate())
                .nickname(userProfile.getNickname())
                .profileImage(userProfile.getProfileImage())
                .description(userProfile.getDescription()).build();
    }

    public static FollowVO from(Follow follow, Boolean isFollowing, Boolean isFollower, ProfileVO userProfile) {
        return FollowVO.builder().uid(userProfile.getUid())
                .approved(follow.getStatus())
                .isFollowing(isFollowing)
                .isFollower(isFollower)
                .approvalDate(follow.getApprovalDate())
                .nickname(userProfile.getNickname())
                .profileImage(userProfile.getProfileImage())
                .description(userProfile.getDescription()).build();
    }
}
