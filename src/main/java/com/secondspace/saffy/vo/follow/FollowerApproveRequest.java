package com.secondspace.saffy.vo.follow;

import com.secondspace.saffy.vo.model.Follow;
import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FollowerApproveRequest {
    @NotNull
    String uid;
    @NonNull
    Follow.Status type;
}
