package com.secondspace.saffy.vo.feed;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.Article;
import com.secondspace.saffy.vo.response.PageToken;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleVO {
    @PageToken
    private Integer id;

    private Article.Status status;
    private boolean isPosted;

    private String url;
    private String title;
    private String description;
    private String mainImage;

    private List<String> keywords;

    private LocalDateTime createDate;

    public static ArticleVO from(Article a) {
        if (a == null) return null;
        return ArticleVO.builder().id(
                a.getId())
                .status(a.getStatus())
                .isPosted(a.isPosted())
                .url(a.getUrl())
                .title(a.getTitle())
                .description(a.getDescription())
                .mainImage(a.getMainImage()).createDate(a.getCreateDate()).build();
    }
}
