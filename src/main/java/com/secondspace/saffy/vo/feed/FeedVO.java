package com.secondspace.saffy.vo.feed;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.secondspace.saffy.vo.model.Article;
import com.secondspace.saffy.vo.model.Feed;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageToken;
import com.secondspace.saffy.vo.user.ProfileVO;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FeedVO {

    @PageToken
    private Integer id;
    private ProfileVO owner;

    @JsonProperty("isScrap")
    private boolean isScrap;

    private String message;
    private ArticleVO article;

    private FeedStatistics statistics;
    private LocalDateTime createDate;


    public static FeedVO from(User u, Feed f, Article a, List<User> likeUsers, int commentCount) {
        return FeedVO.builder()
                .id(f.getId())
                .message(f.getMessage()).createDate(f.getCreateDate())
                .owner(ProfileVO.from(u))
                .article(ArticleVO.from(a))
                .isScrap(f.getByScrap() != null ? f.getByScrap() : false)
                .statistics(FeedStatistics.from(f, likeUsers, commentCount))
                .build();
    }

    @Data
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class FeedStatistics {
        private int viewCount;
        private int scrapCount;
        private int shareCount;
        private int likeCount;
        private int commentCount;

        private boolean liked;

        private List<ProfileVO> likeUsers;

        public static FeedStatistics from(Feed f, List<User> users, int commentCount) {
            return FeedStatistics.builder()
                    .viewCount(f.getViewCount())
                    .scrapCount(f.getScrapCount())
                    .shareCount(f.getShareCount())
                    .likeCount(users.size())
                    .likeUsers(users.stream().map(u -> ProfileVO.from(u)).collect(Collectors.toList()))
                    .commentCount(commentCount)
                    .build();
        }
    }
}
