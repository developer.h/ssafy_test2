package com.secondspace.saffy.vo.feed;

import com.secondspace.saffy.vo.model.Comment;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageToken;
import com.secondspace.saffy.vo.user.ProfileVO;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentVO {
    @PageToken
    private int id;
    private boolean deleted;

    private ProfileVO owner;
    private String message;

    private List<CommentVO> subComments;

    private CommentStatistics statistics;
    private LocalDateTime createDate;

    public static CommentVO from(User u, Comment c, List<User> likeUsers, List<CommentVO> subComments) {
        return CommentVO.builder()
                .id(c.getId())
                .deleted(c.isDeleted())
                .owner(ProfileVO.from(u))
                .message(c.getMessage())
                .subComments(subComments != null ? subComments : new ArrayList<>())
                .statistics(CommentStatistics.from(likeUsers != null ? likeUsers : new ArrayList<>()
                        , subComments != null ? subComments.size() : 0))
                .createDate(c.getCreateDate())
                .build();
    }

    @Data
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class CommentStatistics {
        private int likeCount;
        private int commentCount;

        private boolean liked;

        private List<ProfileVO> likeUsers;

        public static CommentStatistics from(List<User> users, int commentCount) {
            return CommentStatistics.builder()
                    .likeCount(users.size())
                    .likeUsers(users.stream().map(u -> ProfileVO.from(u)).collect(Collectors.toList()))
                    .commentCount(commentCount)
                    .build();
        }
    }
}
