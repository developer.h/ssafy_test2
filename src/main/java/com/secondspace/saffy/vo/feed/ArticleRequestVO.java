package com.secondspace.saffy.vo.feed;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.Article;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleRequestVO {
    @NotEmpty
    private String url;
    private String title;
    private Article.Status status;
    private String description;
    private String mainImage;

    @NotNull
    private List<String> keywords;
}
