package com.secondspace.saffy.vo.feed;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommentRequestVO {

    @NotNull
    private String ownerUid;

    @NotNull
    private int feedId;

    private Integer parentCommentId;
    @NotEmpty
    private String message;
}
