package com.secondspace.saffy.vo.feed;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.Article;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleCurationVO {
    private String url;
    private String title;
    private String description;
    private String mainImage;

    private List<String> keywords;

    private LocalDateTime createDate;

    public static ArticleCurationVO from(Article a, List<String> keywords) {
        if (a == null) return null;
        return ArticleCurationVO.builder()
                .url(a.getUrl()).title(a.getTitle())
                .description(a.getDescription())
                .keywords(keywords)
                .mainImage(a.getMainImage()).createDate(a.getCreateDate()).build();
    }
}
