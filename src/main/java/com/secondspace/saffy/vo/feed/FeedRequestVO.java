package com.secondspace.saffy.vo.feed;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FeedRequestVO {
    @NotNull
    private int articleId;
    @NotEmpty
    private String message;
}
