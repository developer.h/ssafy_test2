package com.secondspace.saffy.vo.curation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CurationSetUpdateRequestVO {
    private String name;

    private List<String> addedKeywords;

    private List<String> deletedKeywords;

    private List<Integer> addedSearchSources;

    private List<Integer> deletedSearchSources;
}
