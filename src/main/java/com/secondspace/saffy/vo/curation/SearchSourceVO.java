package com.secondspace.saffy.vo.curation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.SearchSource;
import com.secondspace.saffy.vo.response.PageToken;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchSourceVO {
    @PageToken
    @NotNull
    private int id;

    @NotNull
    private SearchSource.Type type;

    private String title;

    private String rssUrl;
    private String gceId;
    private String gceApiKey;
}
