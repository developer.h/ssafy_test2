package com.secondspace.saffy.vo.curation;

import com.secondspace.saffy.vo.model.SearchSource;
import com.secondspace.saffy.vo.response.PageToken;

import lombok.*;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CurationSetVO {
    @PageToken
    private int id;
    private String name;

    private List<CurationKeywordVO> keywords;
    private List<CurationSourceVO> searchSources;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @ToString
    public static class CurationKeywordVO {
        private String keyword;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @ToString
    public static class CurationSourceVO {
        private int searchSourceId;

        private SearchSource.Type type;
        private String title;

        private String rssUrl;
        private String gceId;
        private String gceApiKey;
    }
}
