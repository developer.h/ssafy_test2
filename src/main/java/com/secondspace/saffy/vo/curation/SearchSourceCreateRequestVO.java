package com.secondspace.saffy.vo.curation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.SearchSource;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchSourceCreateRequestVO {
    private int id;

    @NotNull
    private SearchSource.Type type;
    @NotEmpty
    private String title;

    private String rssUrl;
    private String gceId;
    private String gceApiKey;
}
