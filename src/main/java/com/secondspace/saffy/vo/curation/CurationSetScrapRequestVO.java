package com.secondspace.saffy.vo.curation;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CurationSetScrapRequestVO {
    @NotNull
    private String sourceUid;

    private List<String> addedKeywords;

    private List<Integer> addedSearchSources;
}
