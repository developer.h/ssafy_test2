package com.secondspace.saffy.vo.curation;

import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@ToString
public class CurationSetCreateRequestVO {

    @NotEmpty
    private String name;

    @NotEmpty
    private List<String> keywords;

    private List<Integer> searchSources;
}
