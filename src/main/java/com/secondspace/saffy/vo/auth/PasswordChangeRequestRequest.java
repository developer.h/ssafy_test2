package com.secondspace.saffy.vo.auth;

import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PasswordChangeRequestRequest {
    String userId;
    String email;
}
