package com.secondspace.saffy.vo.auth;

public class JwtTokenResponse {
    String token;

    public JwtTokenResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
