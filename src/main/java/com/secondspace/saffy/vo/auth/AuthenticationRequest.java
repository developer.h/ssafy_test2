package com.secondspace.saffy.vo.auth;

import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

import javax.validation.Valid;

@Valid
@ToString
public class AuthenticationRequest {
    @ApiModelProperty(notes = "소셜 로그인 일 경우 필수")
    String snsUid;
    @ApiModelProperty(notes = "이메일 로그인 일 경우 필수")
    String email;
    @ApiModelProperty(notes = "이메일 로그인 일 경우 필수")
    String password;
    String name = "";
    String snsType = "";

    public String getSnsUid() {
        return snsUid;
    }

    public void setSnsUid(String snsUid) {
        this.snsUid = snsUid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSnsType() {
        return snsType;
    }

    public void setSnsType(String snsType) {
        this.snsType = snsType;
    }
}
