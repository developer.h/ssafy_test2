package com.secondspace.saffy.vo.auth;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Valid
public class GetTokenRequest {
    @NotNull
    @ApiModelProperty(required = true, notes = "mandatory")
    String email;
    @NotNull
    @ApiModelProperty(required = true, notes = "mandatory")
    String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
