package com.secondspace.saffy.vo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.NotiSetting;
import com.secondspace.saffy.vo.model.UserPrivacy;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SettingVO {
    Boolean isFollowAccept;
    Boolean isPublic;
    NotificationSettingVO notification;

    public static SettingVO from(UserPrivacy userPrivacy, NotiSetting notiSetting) {
        Boolean isFollowAccept = userPrivacy == null ? null : userPrivacy.isFollowApproval();
        Boolean isPublic = userPrivacy == null ? null : !(userPrivacy.isPrivateMode());
        return SettingVO.builder()
                .isFollowAccept(isFollowAccept)
                .isPublic(isPublic)
                .notification(NotificationSettingVO.from(notiSetting))
                .build();
    }

    public static SettingVO from(UserPrivacy userPrivacy) {
        Boolean isFollowAccept = userPrivacy == null ? null : userPrivacy.isFollowApproval();
        Boolean isPublic = userPrivacy == null ? null : !(userPrivacy.isPrivateMode());
        return SettingVO.builder()
                .isFollowAccept(isFollowAccept)
                .isPublic(isPublic)
                .build();
    }
}
