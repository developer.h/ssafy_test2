package com.secondspace.saffy.vo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrivacySettingVO {
    Boolean isPublic;
    Boolean isFollowAccept;
}
