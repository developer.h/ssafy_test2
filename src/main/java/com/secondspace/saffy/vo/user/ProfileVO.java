package com.secondspace.saffy.vo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.Follow;
import com.secondspace.saffy.vo.model.NotiSetting;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.model.UserPrivacy;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfileVO {
    private String uid;
    private String nickname;
    private String description;
    private String profileImage;
    private Integer numberOfFollowings;
    private Integer numberOfFollowers;
    private Boolean isFollower;
    private Boolean isFollowing;
    private Follow.Status followingStatus;
    private SettingVO setting;

    public static ProfileVO from(User u) {
        return ProfileVO.builder()
                .uid(u.getUid())
                .nickname(u.getNickname())
                .description(u.getDescription())
                .profileImage(makeUrlFromFile(u.getUid(), u.getProfileImage()))
                .build();
    }

    public static ProfileVO from(User u, int numberOfFollowings, int numberOfFollowers) {
        return ProfileVO.builder().uid(u.getUid()).nickname(u.getNickname())
                .profileImage(makeUrlFromFile(u.getUid(), u.getProfileImage()))
                .description(u.getDescription()).numberOfFollowings(numberOfFollowings)
                .numberOfFollowers(numberOfFollowers).build();
    }

    private static String makeUrlFromFile(String uid, String file) {
        final String host = "http://ec2-15-164-94-232.ap-northeast-2.compute.amazonaws.com:8080";
        //final String host = "http://localhost:9873";
        final String uri = "/user/profileImage?";
        if (file == null)
            return null;

        return host + uri + "userId=" + uid;
    }

    public static ProfileVO from(User user, int numberOfFollowings, int numberOfFollowers,
                                 NotiSetting notiSetting, UserPrivacy userPrivacy) {
        return ProfileVO.builder().uid(user.getUid())
                .profileImage(makeUrlFromFile(user.getUid(), user.getProfileImage()))
                .numberOfFollowers(numberOfFollowers)
                .numberOfFollowings(numberOfFollowings)
                .description(user.getDescription())
                .nickname(user.getNickname())
                .setting(SettingVO.from(userPrivacy, notiSetting))
                .build();
    }

    public static ProfileVO from(User u, UserPrivacy userPrivacy, int numberOfFollowings,
                                  int numberOfFollowers, boolean isFollower, boolean isFollowing) {
        return ProfileVO.builder().uid(u.getUid())
                .profileImage(makeUrlFromFile(u.getUid(), u.getProfileImage()))
                .numberOfFollowers(numberOfFollowers)
                .numberOfFollowings(numberOfFollowings)
                .description(u.getDescription())
                .nickname(u.getNickname())
                .setting(SettingVO.from(userPrivacy))
                .isFollower(isFollower).isFollowing(isFollowing).build();
    }

    public static ProfileVO from(User u, UserPrivacy userPrivacy, int numberOfFollowings,
                                 int numberOfFollowers, boolean isFollower, boolean isFollowing, Follow.Status  status) {
        return ProfileVO.builder().uid(u.getUid())
                .profileImage(makeUrlFromFile(u.getUid(), u.getProfileImage()))
                .numberOfFollowers(numberOfFollowers)
                .numberOfFollowings(numberOfFollowings)
                .description(u.getDescription())
                .nickname(u.getNickname())
                .setting(SettingVO.from(userPrivacy))
                .isFollower(isFollower).isFollowing(isFollowing).followingStatus(status).build();
    }

}
