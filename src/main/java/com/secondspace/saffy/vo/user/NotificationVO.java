package com.secondspace.saffy.vo.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.secondspace.saffy.vo.model.Notification;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.PageToken;

import lombok.*;

import java.time.LocalDateTime;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationVO {
    private static final JsonMapper JSON_MAPPER = new JsonMapper();
    @PageToken
    private int id;

    private ProfileVO sender;
    private Notification.Type notiType;

    private JsonNode data;

    private LocalDateTime createDate;

    public static NotificationVO from(Notification n, User sender) throws JsonProcessingException {
        return NotificationVO.builder()
                .id(n.getId())
                .sender(ProfileVO.from(sender))
                .notiType(n.getNotiType())
                .data(JSON_MAPPER.readTree(n.getData()))
                .createDate(n.getCreateDate())
                .build();
    }
}
