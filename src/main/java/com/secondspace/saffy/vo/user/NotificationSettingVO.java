package com.secondspace.saffy.vo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.secondspace.saffy.vo.model.NotiSetting;
import lombok.*;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationSettingVO {
    Boolean cancelFollower;
    Boolean followerNewFeed;
    Boolean myFeedComment;
    Boolean myFeedLike;
    Boolean myFeedScrap;
    Boolean newFollower;
    Boolean newFollowerAccept;
    Boolean newFollowerAcceptResult;

    public static NotificationSettingVO from(NotiSetting n) {
        if (n == null) {
            return null;
        }
        return NotificationSettingVO.builder()
                .cancelFollower(n.isCancelFollower())
                .followerNewFeed(n.isFollowerNewFeed())
                .myFeedComment(n.isMyFeedComment())
                .myFeedLike(n.isMyFeedLike())
                .myFeedScrap(n.isMyFeedScrap())
                .newFollower(n.isNewFollower())
                .newFollowerAccept(n.isNewFollowerAccept())
                .newFollowerAcceptResult(n.isNewFollowerAcceptResult()).build();
    }
}
