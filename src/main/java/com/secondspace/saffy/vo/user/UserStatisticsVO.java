package com.secondspace.saffy.vo.user;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserStatisticsVO {
    @Id
    private String uid;

    private int follow;
    private int follower;

    private int feed;
    private int curationSet;
    private int scrapIn;
    private int scrapOut;
    private int likeIn;
    private int likeOut;
    private int commentIn;
    private int commentOut;
}
