package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Feed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String uid;
    private int articleId;

    private Boolean byScrap;

    private String message;

    private int viewCount;
    private int scrapCount;
    private int shareCount;

    public void increaseScrapCount(){
        scrapCount++;
    }

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;
}
