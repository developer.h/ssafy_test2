package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeedLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int feedId;
    private Type type;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;

    public enum Type {
        LIKE(0), VIEW(1), SHARE(2);
        private int value;

        Type(int value) {
            this.value = value;
        }
    }
}
