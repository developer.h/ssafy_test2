package com.secondspace.saffy.vo.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class UserSns {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String uid;
    private SnsType snsType;
    private String snsUid;
    private String accessToken;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;

    public enum SnsType {
        NAVER(0), KAKAO(1), GOOGLE(2);

        private int value;

        SnsType(int value) {
            this.value = value;
        }
    }
}
