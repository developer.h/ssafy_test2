package com.secondspace.saffy.vo.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String uid;
    private Status status;
    private boolean isPosted;
    private String url;
    private String title;
    private String description;
    private String mainImage;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;

    @Getter
    public enum Status {
        SCRAP(0), SKIP(1);

        private int value;

        Status(int value) {
            this.value = value;
        }
    }
}
