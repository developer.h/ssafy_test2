package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NotiSetting {
    @Id
    private String uid;

    boolean cancelFollower;
    boolean followerNewFeed;
    boolean myFeedComment;
    boolean myFeedLike;
    boolean myFeedScrap;
    boolean newFollower;
    boolean newFollowerAccept;
    boolean newFollowerAcceptResult;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;

    public static final NotiSetting DEFAULT_NOTI_SETTING = NotiSetting.builder()
            .cancelFollower(true)
            .followerNewFeed(true)
            .myFeedComment(true)
            .myFeedLike(true)
            .myFeedScrap(true)
            .newFollower(true)
            .newFollowerAccept(true)
            .newFollowerAcceptResult(true)
            .build();
}
