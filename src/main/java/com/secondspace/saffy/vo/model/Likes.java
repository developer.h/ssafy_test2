package com.secondspace.saffy.vo.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Likes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String uid;
    private String targetUid;

    private TargetType targetType;
    private int targetId;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;

    @Getter
    public enum TargetType {
        FEED(0), COMMENT(1);

        private int value;

        TargetType(int value) {
            this.value = value;
        }
    }
}
