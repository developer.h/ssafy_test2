package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CurationSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String uid;
    private String name;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;
}
