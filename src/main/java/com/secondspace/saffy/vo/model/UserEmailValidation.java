package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEmailValidation {
    @Id
    String code;

    String uid;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;
}
