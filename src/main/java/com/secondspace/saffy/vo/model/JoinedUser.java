package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JoinedUser {
    @Id
    private String uid;
    private String password;
    private String email;
    private String nickname;
    private String description;
    private String profileImage;
    private boolean emailCheck;
    private String snsUid;
    private Integer snsType;
}
