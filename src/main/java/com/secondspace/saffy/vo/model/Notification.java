package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String uid;
    private String senderUid;
    private Type notiType;

    private String data;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;
    @Column(insertable = false, updatable = false)
    private LocalDateTime updateDate;

    public enum Type {
        cancelFollower(0), followerNewFeed(1),
        myFeedComment(2), myFeedLike(3), myFeedScrap(4),
        newFollower(5), newFollowerAccept(6), newFollowerAcceptResult(7);

        private int value;

        Type(int value) {
            this.value = value;
        }
    }
}
