package com.secondspace.saffy.vo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String uid;

    private String resultUid;
    private String keyword;

    @Column(insertable = false, updatable = false)
    private LocalDateTime createDate;

    public enum Type {
        USER(0), KEYWORD(1);
        private int value;

        Type(int value) {
            this.value = value;
        }
    }
}
