package com.secondspace.saffy.config;

import com.secondspace.saffy.constant.ApiConstant;
import com.secondspace.saffy.constant.AppConstants;
import com.secondspace.saffy.exception.SaffyException;
import com.secondspace.saffy.service.UserService;
import com.secondspace.saffy.vo.model.User;
import com.secondspace.saffy.vo.response.RCode;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class UserInterceptor extends HandlerInterceptorAdapter {

    private final UserService userService;

    public UserInterceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("Interceptor > preHandle");
        String userName = (String) request.getAttribute(AppConstants.userUid);
        String api = request.getMethod() +" "+ request.getRequestURI();
        log.info("API = "+api);
        User requestUser = userService.getUser(userName);
        request.setAttribute(AppConstants.REQUEST_USER, requestUser);

        String targetUid = request.getParameter(ApiConstant.PARAM_TARGET_USER_ID);
        if (!StringUtils.isEmpty(targetUid)) {
            User targetUser = targetUid.equals(userName) ? requestUser : userService.getUser(targetUid);
            if (targetUser == null) {
                throw new SaffyException(RCode.USER_NOT_EXISTS);
            }
            request.setAttribute(AppConstants.REQUESTED_TARGET_USER, targetUser);
        } else {
            request.setAttribute(AppConstants.REQUESTED_TARGET_USER, requestUser);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("Interceptor > postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception arg3) throws Exception {
        log.info("Interceptor > afterCompletion" );
    }
}