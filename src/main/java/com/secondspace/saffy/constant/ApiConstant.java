package com.secondspace.saffy.constant;

public class ApiConstant {
    public static final String paramPassword = "password";
    public static final String paramEmail = "email";
    public static final String paramSnsUid = "snsUid";
    public static final String socialTypeGoogle = "google";
    public static final String socialTypeFacebook = "facebook";
    public static final String socialTypeKAKAO = "KAKAO";
    public static final String socialTypeNaver = "naver";

    public static final String socialType = "socialType";

    public static final String PARAM_TARGET_USER_ID = "uid";
    public static final String PARAM_SCRAP_SOURCE_UID = "scrapSourceUid";
    public static final String PARAM_ARTICLE_STATUS = "status";
    public static final String PARAM_SEARCH_QUERY = "query";
    public static final String PARAM_SEARCH_KEYWORD = "keyword";
    public static final String IS_FOLLOW_ACCEPT = "isFollowAccept";
    public static final String IS_PUBLIC = "isPublic";

    public static enum FeedSortParam {
        COMMENT("comment"), LIKE("like"), SCRAP("scrap");
        String value;

        private FeedSortParam(String value) {
            this.value = value;
        }
    }

    public static final String PARAM_LIKE_TARGET_TYPE = "targetType";
    public static final String PARAM_LIKE_TARGET_ID = "targetId";
    public static final String PARAM_LIKE_ONOFF = "liked";


    public static final String PARAM_PRIVATE_MODE = "privateMode";
    public static final String PARAM_AUTO_FOLLOW_APPROVAL = "autoFollowApproval";

    public static final String PARAM_NOTI_SETTING_COMMENT = "comment";
    public static final String PARAM_NOTI_SETTING_LIKE = "like";
    public static final String PARAM_NOTI_SETTING_SCRAP = "scrap";
    public static final String PARAM_NOTI_SETTING_FOLLOWING_NEW = "followingNew";

    public static enum FollowApprovedResult {
        REQUESTED(0, "REQUESTED"), APPROVED(1, "APPROVED"), DENIED(2, "DENIED");

        private int value;
        private String valueStr;

        FollowApprovedResult(int value, String valueStr) {
            this.value = value;
            this.valueStr = valueStr;
        }

        public String getValueStr() {
            return valueStr;
        }
    }

}
