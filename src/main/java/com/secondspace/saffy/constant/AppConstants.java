package com.secondspace.saffy.constant;

public class AppConstants {
    public static final String userUid = "userUid";
    public static final String REQUEST_USER = "Request-User";
    public static final String REQUESTED_TARGET_USER = "Requested-Target-User";
}
